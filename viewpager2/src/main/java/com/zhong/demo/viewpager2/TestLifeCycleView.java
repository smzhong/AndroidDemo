package com.zhong.demo.viewpager2;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;

/**
 * <pre>
 *     author: Zhong
 *     time  : 2020/4/25
 *     desc  :
 * </pre>
 */
@SuppressLint("AppCompatCustomView")
public class TestLifeCycleView extends TextView {
    private static final String TAG = TestLifeCycleView.class.getSimpleName();

    private LifecycleObserver lifeCycleObserver;

    public TestLifeCycleView(Context context) {
        this(context, null);
    }

    public TestLifeCycleView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TestLifeCycleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public LifecycleObserver getLifeCycleObserver(){
        if(lifeCycleObserver == null){
            lifeCycleObserver = new LifecycleObserver() {
                @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
                public void onLifeOnCreate(){
                    Log.d(TAG, "onLifeOnCreate");
                }

                @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
                public void onLifeOnResume(){
                    Log.d(TAG, "onLifeOnResume");
                }

                @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
                public void onLifePause(){
                    Log.d(TAG, "onLifePause");
                }

                @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
                public void onLifeDestroy(){
                    Log.d(TAG, "onLifeDestroy");
                }
            };
        }

        return lifeCycleObserver;
    }
}
