package com.zhong.demo.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.zhong.demo.fragment.fragment.AFragment;
import com.zhong.demo.fragment.fragment.BFragment;
import com.zhong.demo.fragment.fragment.CFragment;
import com.zhong.demo.fragment.fragment.FragmentAdapter;
import com.zhong.demo.fragment.widget.TVTabLayout;

import java.util.ArrayList;
import java.util.List;

public class TVTabLayoutActivity extends AppCompatActivity {
    private static final String TAG = TVTabLayoutActivity.class.getSimpleName();

    private TVTabLayout tabLayout;

    private ViewPager viewPager;
    private FragmentPagerAdapter adapter;
    private List<Fragment> fragmentList;

    private List<TextView> tabViewList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tvtab_layout);

        initView();
        initData();
        initViewPager();
        initTabLayout();

        tabLayout.requestFocus();
    }

    private void initView() {
        tabLayout = findViewById(R.id.tv_tab_layout);
        viewPager = findViewById(R.id.man_view_pager);
    }

    private void initTabLayout(){
        for(int i = 0; i < fragmentList.size(); i++){
            TextView tabView = (TextView) getTabView("TAG" + i);
            tabViewList.add(tabView);
            TVTabLayout.TabView tab = tabLayout.newTabView().setCustomView(tabView);
            tabLayout.addTab(tab);
        }
        //绑定ViewPager
        tabLayout.setupWithViewPager(viewPager);
        //设置Tab焦点框
        tabLayout.setTabFocusedBackground(R.drawable.focused_bg);
        //设置tab间距
        tabLayout.setTabsMargin(10);
        //设置模式：选中Tab居中模式
        tabLayout.setMode(TVTabLayout.MODE_SCROLLABLE_INDICATOR_CENTER);
        //添加监听，处理TabView的UI效果
        tabLayout.addOnTabSelectedListener(new TVTabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TVTabLayout.TabView view, int index) {
                Log.d(TAG, "onTabSelected index = " + index);
                tabViewList.get(index).setTextColor(Color.RED);
            }

            @Override
            public void onTabUnSelected(TVTabLayout.TabView view, int index) {
                Log.d(TAG, "onTabUnSelected index = " + index);
                tabViewList.get(index).setTextColor(Color.BLACK);
            }
        });
    }

    /**
     * 获取TabView
     * @param title
     * @return
     */
    private View getTabView(String title){
        TextView customView = new TextView(TVTabLayoutActivity.this);
        customView.setPadding(15,7,15,7);
        customView.setGravity(Gravity.CENTER);
        customView.setTextColor(Color.BLACK);
        customView.setText(title);
        return customView;
    }

    private void initViewPager(){
        if(adapter == null){
            adapter = new FragmentAdapter(getSupportFragmentManager(),fragmentList);
            viewPager.setAdapter(adapter);
        }
    }

    private void initData(){
        fragmentList = new ArrayList<>();
        fragmentList.add(new AFragment());
        fragmentList.add(new BFragment());
        fragmentList.add(new CFragment());
        fragmentList.add(new AFragment());
        fragmentList.add(new BFragment());
        fragmentList.add(new CFragment());
        fragmentList.add(new AFragment());
        fragmentList.add(new BFragment());
        fragmentList.add(new CFragment());
    }
}
