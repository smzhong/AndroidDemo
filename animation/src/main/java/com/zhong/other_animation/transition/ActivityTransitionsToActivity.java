package com.zhong.other_animation.transition;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.zhong.main.BaseActivity;
import com.zhong.main.R;

import butterknife.BindView;

public class ActivityTransitionsToActivity extends BaseActivity {

    @BindView(R.id.tool_bar)
    Toolbar toolbar;

    public static void startSelf(Context context){
        Intent intent = new Intent(context, ActivityTransitionsToActivity.class);
        context.startActivity(intent, ActivityOptions.makeSceneTransitionAnimation((Activity) context).toBundle());
    }

    @Override
    protected int initLayout() {
        return R.layout.activity_transitions_to;
    }

    @Override
    protected void create() {
        initToolBar();
    }

    private void initToolBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finishAfterTransition();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        finishAfterTransition();
    }
}
