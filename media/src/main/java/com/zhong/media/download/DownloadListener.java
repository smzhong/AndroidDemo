package com.zhong.media.download;

/**
 * Author: Zhong
 * Date: 2017/6/12
 * Email: zhongshim123@163.com
 */
public interface DownloadListener {
    void onProgress(int progres, String fileName);
    void onError(String msg);
    void onComplete();
}
