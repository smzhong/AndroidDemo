package com.zhong.mvp2.bean;

/**
 * <pre>
 *     author: Zhong
 *     time  : 2018/12/8
 *     desc  :
 * </pre>
 */
public class UserBean {
    private String userName;
    private String password;

    //模拟不同的登录状态
    private String loginResultType = "1";
    private String token;

    public UserBean() {
    }

    public UserBean(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public UserBean(String userName, String password, String loginResultType, String token) {
        this.userName = userName;
        this.password = password;
        this.loginResultType = loginResultType;
        this.token = token;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLoginResultType() {
        return loginResultType;
    }

    public void setLoginResultType(String loginResultType) {
        this.loginResultType = loginResultType;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "UserName=" + userName
                + "\n Password=" + password
                + "\n token=" + token;
    }
}
