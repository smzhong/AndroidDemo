package com.zhong.encrypt;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.zhong.encrypt.Utils.MD5Util;


public class MD5Activity extends AppCompatActivity {
    private EditText mEditText;
    private TextView md5TextView;
    private Button md5Button;
    private TextView logTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_md5);

        initView();
    }

    private void initView() {
        mEditText = findViewById(R.id.text_et);
        md5TextView = findViewById(R.id.secret_tv);
        md5Button = findViewById(R.id.secret_btn);
        logTextView = findViewById(R.id.log_tv);

        md5Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strMD5 = mEditText.getText().toString().trim();
                if(TextUtils.isEmpty(strMD5)){
                    return;
                }

                MD5Util md = new MD5Util();

                showLog("原文：\n" + strMD5, false);
                showLog("东东的：\n" + md.getStrrMD5(strMD5), true);
                showLog("MD5后：\n" + md.getStrMD5(strMD5), true);
                showLog("加密的：\n" + md.getconvertMD5(strMD5), true);
                showLog("加盐加密的：\n" + md.getSaltMD5(strMD5), true);
                showLog("加盐后是否与原文一致：\n" + Boolean.toString(md.getSaltverifyMD5(strMD5, md.getSaltMD5(strMD5))), true);

            }
        });
    }


    private void showLog(String log, boolean isAppend){
        if(isAppend){
            log = logTextView.getText().toString() + log + "\n";
        } else{
            log += "\n";
        }

        logTextView.setText(log);
    }

}
