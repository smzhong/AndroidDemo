package com.zhong.mvp2.presenter;

import android.content.Context;

import com.zhong.mvp2.model.IModel;
import com.zhong.mvp2.view.IView;

/**
 * <pre>
 *     author: Zhong
 *     time  : 2018/12/14
 *     desc  :
 * </pre>
 */
public abstract class BasePresenter<V extends IView, M extends IModel> implements IPresenter{

    protected V mView;
    protected M mModel;

    public BasePresenter(){
        mModel = initModel();
    }

    @Override
    public void attachView(IView view) {
        mView = (V) view;
    }

    protected abstract M initModel();

    @Override
    public void detachView() {
        mView = null;
        mModel = null;
    }
}
