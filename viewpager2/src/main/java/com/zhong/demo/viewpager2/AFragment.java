package com.zhong.demo.viewpager2;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class AFragment extends Fragment {
    private static final String TAG = AFragment.class.getSimpleName();
    private View view;
    private Button testBtn;

    private String fragmentName ;

    public AFragment(String fragmentName){
        this.fragmentName = fragmentName;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, fragmentName + "  onCreateView");
        view = inflater.inflate(R.layout.fragment_a, null);

        initView();

        return view;
    }

    protected void initView() {
        testBtn = view.findViewById(R.id.start_test_activity_btn);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, fragmentName +  "  onCreate");
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG, fragmentName +  "  onDestroyView");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, fragmentName +  "  onActivityCreated");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, fragmentName +  "  onStart");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, fragmentName +  "  onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, fragmentName + "  onPause");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, fragmentName +  "  onDestroy");
    }
}
