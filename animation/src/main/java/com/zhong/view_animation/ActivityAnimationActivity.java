package com.zhong.view_animation;

import com.zhong.main.BaseActivity;
import com.zhong.main.R;

public class ActivityAnimationActivity extends BaseActivity {


    @Override
    protected int initLayout() {
        return R.layout.activity_activity_animation;
    }

    @Override
    protected void create() {
        loadToolbar();
    }
}
