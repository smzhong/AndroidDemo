package com.zhong.demo.butterknife;

import android.widget.Button;

import com.zhong.demo.butterknife.base.BaseFragment;

import butterknife.BindView;

/**
 * <pre>
 *     author: Zhong
 *     time  : 2019/2/21
 *     desc  :
 * </pre>
 */
public class BindViewFragment extends BaseFragment {

    @BindView(com.zhong.demo.butterknife.R.id.bind_btn)
    Button bindBtn;

    @Override
    protected int initLayout() {
        return com.zhong.demo.butterknife.R.layout.fragment_bind_view;
    }
}
