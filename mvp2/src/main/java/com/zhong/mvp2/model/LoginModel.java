package com.zhong.mvp2.model;

import android.os.Handler;
import android.os.Looper;

import com.zhong.mvp2.bean.UserBean;
import com.zhong.mvp2.contract.LoginContract;


/**
 * <pre>
 *     author: Zhong
 *     time  : 2018/12/8
 *     desc  :
 * </pre>
 */
public class LoginModel implements IModel,LoginContract.ILoginModel {
    private Handler handler;

    public LoginModel(){
        handler = new Handler(Looper.getMainLooper());
    }

    @Override
    public void doLogin(final UserBean param, final LoginCallBack loginCallBack) {
        loginCallBack.onComplete();

        //模拟登录延迟操作
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                switch (param.getLoginResultType()){
                    case "1":
                        param.setToken("登录成功");
                        loginCallBack.onSuccess(param);
                        break;
                    case "2":
                        loginCallBack.onFailure("用户名或密码错误");
                        break;
                    case "3":
                        loginCallBack.onError("接口超时");
                        break;
                }
            }
        }, 3000);
    }
}
