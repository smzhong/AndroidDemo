package com.zhong.property_animation;

import android.animation.LayoutTransition;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.zhong.main.R;

import java.util.LinkedList;
import java.util.Queue;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LayoutTransitionActivity extends AppCompatActivity {

    @BindView(R.id.button_lin)
    ViewGroup rootView;
    @BindView(R.id.button)
    Button button;
    @BindView(R.id.button2)
    Button button2;
    @BindView(R.id.button3)
    Button button3;
    @BindView(R.id.button4)
    Button button4;

    private Queue<Button> addViewStack= new LinkedList<>();
    private Queue<Button> removeViewStack= new LinkedList<>();

    /** 动画时长 */
    private static final int ANIMATOR_DURATION = 1000;

    public static void startSelf(Context context){
        Intent intent = new Intent(context, LayoutTransitionActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_layout_transition);

        ButterKnife.bind(this);

        addViewStack.add(button4);
        addViewStack.add(button3);
        addViewStack.add(button2);
        addViewStack.add(button);

        setLayoutTransition();
    }

    private void setLayoutTransition(){
        //初始化
        LayoutTransition layoutTransition = new LayoutTransition();

        //统一设置LayoutTransition的动画时间， ANIMATOR_DURATION定义的常量
        layoutTransition.setDuration(LayoutTransition.DISAPPEARING,ANIMATOR_DURATION);
        layoutTransition.setDuration(LayoutTransition.APPEARING, ANIMATOR_DURATION);
        layoutTransition.setDuration(LayoutTransition.CHANGE_DISAPPEARING, ANIMATOR_DURATION);
        layoutTransition.setDuration(LayoutTransition.CHANGE_APPEARING, ANIMATOR_DURATION);

        //view 动画改变时，布局中的每个子view动画的时间间隔
        layoutTransition.setStagger(LayoutTransition.CHANGE_APPEARING, 1000);
        layoutTransition.setStagger(LayoutTransition.CHANGE_DISAPPEARING, 1000);

        //设置APPEARING行为
        ObjectAnimator appearingAnimator = ObjectAnimator.ofFloat(rootView,"scaleY", 0, 1);
        appearingAnimator.setDuration(layoutTransition.getDuration(LayoutTransition.APPEARING));
        layoutTransition.setAnimator(LayoutTransition.APPEARING,appearingAnimator);

        //设置DISAPPEARING行为
        ObjectAnimator disAppearingAnimator = ObjectAnimator.ofFloat(rootView,"scaleY", 1,0);
        disAppearingAnimator.setDuration(layoutTransition.getDuration(LayoutTransition.DISAPPEARING));
        layoutTransition.setAnimator(LayoutTransition.DISAPPEARING,disAppearingAnimator);

        PropertyValuesHolder pvhLeft =
                PropertyValuesHolder.ofInt("left", 0, 0);
        PropertyValuesHolder pvhTop =
                PropertyValuesHolder.ofInt("top", 0, 0);
        PropertyValuesHolder pvhRight =
                PropertyValuesHolder.ofInt("right", 0, 0);
        PropertyValuesHolder pvhBottom =
                PropertyValuesHolder.ofInt("bottom", 0, 0);

        //设置CHANGE_APPEARING行为
        PropertyValuesHolder animator = PropertyValuesHolder.ofFloat("rotation", 0, 90, 0);
        final ObjectAnimator changeIn = ObjectAnimator.ofPropertyValuesHolder(
                this, pvhTop, pvhBottom,pvhLeft, pvhRight, animator).
                setDuration(layoutTransition.getDuration(LayoutTransition.CHANGE_APPEARING));
        layoutTransition.setAnimator(LayoutTransition.CHANGE_APPEARING, changeIn);

        //设置CHANGE_DISAPPEARING
        PropertyValuesHolder pvhRotation =
                PropertyValuesHolder.ofFloat("scaleX", 1, 1.5f, 1);
        final ObjectAnimator changeOut = ObjectAnimator.ofPropertyValuesHolder(
                this, pvhTop, pvhBottom,pvhLeft, pvhRight, pvhRotation).
                setDuration(layoutTransition.getDuration(LayoutTransition.CHANGE_DISAPPEARING));
        layoutTransition.setAnimator(LayoutTransition.CHANGE_DISAPPEARING, changeOut);

        rootView.setLayoutTransition(layoutTransition);
    }

    @OnClick({R.id.add_btn,R.id.removie_btn, R.id.button, R.id.button2, R.id.button3, R.id.button4})
    protected void onClick(View view){
        switch (view.getId()){
            case R.id.add_btn:
                if(addViewStack.size() > 0){
                    Button btn = addViewStack.poll();
                    btn.setVisibility(View.VISIBLE);
                    removeViewStack.add(btn);
                }
                break;
            case R.id.removie_btn:
                if(removeViewStack.size() > 0){
                    Button btn = removeViewStack.poll();
                    btn.setVisibility(View.GONE);
                    addViewStack.add(btn);
                }
                break;
            case R.id.button:
                button.setVisibility(View.GONE);

                removeViewStack.remove(button);
                addViewStack.add(button);
                break;
            case R.id.button2:
                button2.setVisibility(View.GONE);

                removeViewStack.remove(button2);
                addViewStack.add(button2);
                break;
            case R.id.button3:
                button3.setVisibility(View.GONE);

                removeViewStack.remove(button3);
                addViewStack.add(button3);
                break;
            case R.id.button4:
                button4.setVisibility(View.GONE);

                removeViewStack.remove(button4);
                addViewStack.add(button4);
                break;
        }
    }
}
