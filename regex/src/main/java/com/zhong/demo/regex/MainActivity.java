package com.zhong.demo.regex;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    @BindView(R.id.pattern_et)
    EditText patternEt;
    @BindView(R.id.source_et)
    EditText sourceEt;
    @BindView(R.id.log_tv)
    TextView logTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
    }

    @OnClick({R.id.clear_btn, R.id.regex_btn, R.id.data_mate_btn, R.id.data_parity_btn})
    protected void onClick(View v) {
        String pattern = patternEt.getText().toString();
        String source = sourceEt.getText().toString();

        switch (v.getId()) {
            case R.id.clear_btn:
                patternEt.setText("");
                sourceEt.setText("");
                logTv.setText("");
                break;
            case R.id.regex_btn:
                if (TextUtils.isEmpty(pattern) || TextUtils.isEmpty(source)) {
                    Toast.makeText(MainActivity.this, "不能为空啊，大哥", Toast.LENGTH_SHORT).show();
                    return;
                }

                try {
                    boolean result = regex(source, pattern);
                    showLog(source, pattern, String.valueOf(result));
                } catch (PatternSyntaxException ex) {
                    ex.printStackTrace();
                    Toast.makeText(MainActivity.this, "错误的正则表达式", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.data_mate_btn:
                //数据匹配
                try {
                    mateRegex(source, pattern);
                } catch (PatternSyntaxException ex) {
                    ex.printStackTrace();
                    Toast.makeText(MainActivity.this, "错误的正则表达式", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.data_parity_btn:
                //数据校验
                if (TextUtils.isEmpty(pattern) || TextUtils.isEmpty(source)) {
                    Toast.makeText(MainActivity.this, "不能为空啊，大哥", Toast.LENGTH_SHORT).show();
                    return;
                }
                try {
                    parityRegex(source, pattern);
                } catch (PatternSyntaxException ex) {
                    ex.printStackTrace();
                    Toast.makeText(MainActivity.this, "错误的正则表达式", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private boolean regex(String sourceStr, String patternStr) throws PatternSyntaxException {
        Pattern pattern = Pattern.compile(patternStr);
        Matcher matcher = pattern.matcher(sourceStr);

        return matcher.matches();
    }

    private boolean parityRegex(String sourceStr, String patternStr) throws PatternSyntaxException {
        boolean result = false;
        showLog("patternStr: " + patternStr + "  sourceStr: " + sourceStr);

        /** 校验数据格式 **/
        //1.可以使用String中的matches方法，校验字符串是否满足表达式要求
        result = sourceStr.matches(patternStr);
        showLog(String.valueOf(result));

        //2.使用Pattern类中的matches方法
        //Pattern是正则表达式在Java中的编译表示
        result = Pattern.matches(patternStr, sourceStr);

        //提供了split方法，匹配pattern进行分割字符串
        String[] splitResult = Pattern.compile(patternStr).split(sourceStr);
        //String中的replaceAll和split方法都支持正则表达式
        String newStr = sourceStr.replaceAll(patternStr, "数字");
        showLog(newStr);
        String[] strSplitResult = sourceStr.split(patternStr);

        //3.使用Matcher中的matches方法
        Pattern matPattern = Pattern.compile(patternStr);
        //Matcher类封装了一系列对字符串进行解释和匹配操作的方法
        Matcher matcher = matPattern.matcher(sourceStr);
        result = matcher.matches();

        return result;
    }

    private void mateRegex(String sourceStr, String patternStr) throws PatternSyntaxException {
        Pattern matPattern = Pattern.compile(patternStr);
        Matcher matcher = matPattern.matcher(sourceStr);

        //匹配成功
        int group = 0;
        while (matcher.find()) {
            group++;
            showLog("匹配:" + matcher.group());
        }
    }

    private void showLog(String sourceStr, String patternStr, String result) {
        StringBuffer stringBuffer = new StringBuffer(logTv.getText().toString());
        stringBuffer.append("patternStr:" + patternStr + "\n");
        stringBuffer.append("source:" + sourceStr + "\n");
        stringBuffer.append("result:" + result + "\n");

        Log.d(TAG, stringBuffer.toString());
        logTv.setText(stringBuffer.toString());
    }

    private void showLog(String log) {
        StringBuffer stringBuffer = new StringBuffer(logTv.getText().toString());
        stringBuffer.append(log + "\n");

        Log.d(TAG, stringBuffer.toString());
        logTv.setText(stringBuffer.toString());
    }
}
