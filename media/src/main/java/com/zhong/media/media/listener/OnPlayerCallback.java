package com.zhong.media.media.listener;

import android.net.Uri;

/**
 * 视频播放状态监听
 */
public interface OnPlayerCallback {
    /**
     * 开始播放
     * @param mUri video's path
     * @param index video's index in video list
     */
    void onVideoStart(Uri mUri, int index);
    /**
     * 播放完成
     * @param mUri video's path
     * @param index video's index in video list
     */
    void onVideoComplete(Uri mUri, int index);
    /**
     * 播放出错
     * @param mUri video's path
     * @param index video's index in video list
     */
    void onVideoError(Uri mUri, int index);
    /**
     * 播放停止
     *      播放异常，重试次数结束之后停止播放
     */
    void onVideoStop(int retryCount, Uri mUri, int index);

    /**
     * 视频窗口窗口状态，全屏或小窗口
     * @param isFullScreen Is video fullscreen
     */
    void onVideoSizeChangedListener(boolean isFullScreen);
    /**
     * 视频加载状态变化
     *
     * @param isShow 是否显示loading
     */
    void onLoadingChanged(boolean isShow);
}
