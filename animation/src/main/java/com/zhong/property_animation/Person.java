package com.zhong.property_animation;

/**
 * <pre>
 *     author: Zhong
 *     time  : 2019/6/5
 *     desc  :
 * </pre>
 */
public class Person {
    private String name;
    private int age;
    private String sex;

    private static Person mPerson;

    private Person() {
    }

    //单例
    public static Person getInstance() {
        if (mPerson == null) {
            mPerson = new Person();
        }

        return mPerson;
    }

    public Person setName(String name) {
        this.name = name;
        return this;
    }

    public Person setAge(int age) {
        this.age = age;
        return this;
    }

    public Person setSex(String sex) {
        this.sex = sex;
        return this;
    }
}
