package com.zhonge.androiddemo;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.zhonge.androiddemo.manager.ActivityManager;
import com.zhonge.androiddemo.manager.MyApplication;

import java.util.Stack;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        findViewById(R.id.stack_btn).setOnClickListener(onClickListener);
        findViewById(R.id.finish_btn).setOnClickListener(onClickListener);
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.stack_btn:
                    Stack<Activity> activities = ActivityManager.getInstance().getActivityStack();
                    for(Activity activity : activities){
                        Log.d(MyApplication.TAG, "当前Activity任务栈：" + activity.getClass().getSimpleName());
                    }
                    break;
                case R.id.finish_btn:
                    ActivityManager.getInstance().finishActivity(SecondActivity.this);
                    break;
            }
        }
    };
}
