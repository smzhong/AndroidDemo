package com.zhong.mvp.model;

import com.zhong.mvp.bean.UserBean;

/**
 * <pre>
 *     author: Zhong
 *     time  : 2018/12/8
 *     desc  :
 * </pre>
 */
public interface ILoginModel {
    /**
     * 登录操作
     * @param param 参数
     */
    void doLogin(UserBean param, LoginCallBack loginCallBack);

    /**
     * 登录状态回调
     */
    public interface LoginCallBack{
        /**
         * 登录成功
         * @param data 返回数据
         */
        void onSuccess(UserBean data);

        /**
         * 调用登录接口时，接口调用成功，但是
         *      因用户名错误、登录失效等后台控制逻辑导致的登录失败
         * @param data 失败原因
         */
        void onFailure(String data);

        /**
         * 接口调用失败
         *      网络不通
         *      接口超时
         *      404、500等原因
         * @param error 失败原因
         */
        void onError(String error);

        /**
         * 接口请求结束，包括上面三中情况
         *     设置此方法通常是进行hideLoading等操作
         */
        void onComplete();
    }
}
