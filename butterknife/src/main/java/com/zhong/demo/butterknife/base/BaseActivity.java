package com.zhong.demo.butterknife.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;

/**
 * <pre>
 *     author: Zhong
 *     time  : 2019/2/21
 *     desc  : Activity基类
 * </pre>
 */
public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(initLayout());

        //在基类中绑定ButterKnife
        ButterKnife.bind(this);

        create();
    }

    /**
     * 返回Activity layout用于绑定View
     * @return Activity layout
     */
    protected abstract int initLayout();

    /**
     * Activity onCreate完成后的回调
     */
    protected abstract void create();
}
