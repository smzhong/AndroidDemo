package com.zhong.view_animation;

import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

import com.zhong.main.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LayoutAnimationActivity extends AppCompatActivity {

    @BindView(R.id.root_view)
    ConstraintLayout rootView;

    public static void startSelf(Context context){
        Intent intent = new Intent(context, LayoutAnimationActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_layout_animation);

        ButterKnife.bind(this);

        Animation animation = AnimationUtils.loadAnimation(this,R.anim.left_to_right_anim);
        LayoutAnimationController controller = new LayoutAnimationController(animation);
        controller.setDelay(0.2f);
        controller.setOrder(LayoutAnimationController.ORDER_REVERSE);
        rootView.setLayoutAnimation(controller);
    }
}
