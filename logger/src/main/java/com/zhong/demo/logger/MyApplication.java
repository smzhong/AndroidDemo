package com.zhong.demo.logger;

import android.app.Application;
import android.support.annotation.Nullable;

import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.CsvFormatStrategy;
import com.orhanobut.logger.DiskLogAdapter;
import com.orhanobut.logger.DiskLogStrategy;
import com.orhanobut.logger.FormatStrategy;
import com.orhanobut.logger.LogcatLogStrategy;
import com.orhanobut.logger.Logger;
import com.orhanobut.logger.PrettyFormatStrategy;

/**
 * <pre>
 *     author: Zhong
 *     time  : 2019/3/5
 *     desc  :
 * </pre>
 */
public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();


//        FormatStrategy formatStrategy = CsvFormatStrategy.newBuilder()
//                .tag("TAG")
//                .build();
//        Logger.addLogAdapter(new DiskLogAdapter(formatStrategy){
//            @Override
//            public boolean isLoggable(int priority, @Nullable String tag) {
//                return BuildConfig.DEBUG;
//            }
//        });

//        Logger.addLogAdapter(new AndroidLogAdapter());

        Logger.addLogAdapter(new AndroidLogAdapter(){
            @Override
            public boolean isLoggable(int priority, @Nullable String tag) {
                //也可以根据priority的VERBOSE、DEBUG、INFO、WARN、ERROR等不同等级
                //进行过滤只在发布版本中保留ERROR的打印
                return BuildConfig.DEBUG;   //只在DEBUG模式下打印log
            }
        });

//        FormatStrategy formatStrategy = PrettyFormatStrategy.newBuilder()
//                .showThreadInfo(true)                   //是否打印当前线程
//                .methodCount(2)                         //方法数显示多少行，默认两行
//                .methodOffset(5)                        //隐藏方法内部调用到偏移量，默认5
//                .tag("Logger Demo")                     //自定义TAG，Logger打印会使用同一个TAG
//                .build();
//        Logger.addLogAdapter(new AndroidLogAdapter(formatStrategy));
    }
}
