package com.zhong.demo.butterknife;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.zhong.demo.butterknife.adapter.RecyclerAdapter;
import com.zhong.demo.butterknife.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class BindAdapterActivity extends BaseActivity {

    @BindView(com.zhong.demo.butterknife.R.id.recycler_view)
    RecyclerView recyclerView;

    private RecyclerAdapter recyclerAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private List<String> data;

    public static void startSelf(Context context){
        Intent intent = new Intent(context, BindAdapterActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected int initLayout() {
        return com.zhong.demo.butterknife.R.layout.activity_bind_adapter;
    }

    @Override
    protected void create() {
        initData();
        initRecycler();
    }

    private void initData() {
       data = new ArrayList<>();
       for (int i = 0; i < 20; i++){
           data.add("Item " + i);
       }
    }

    private void initRecycler() {
        recyclerAdapter = new RecyclerAdapter(BindAdapterActivity.this, data);

        layoutManager = new GridLayoutManager(BindAdapterActivity.this,3,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(recyclerAdapter);
    }
}
