package com.zhong.demo.fragment.fragment;

import android.util.Log;

import com.zhong.demo.fragment.R;
import com.zhong.demo.fragment.fragment.base.BaseLazyLoadFragment;

public class BFragment extends BaseLazyLoadFragment {

    @Override
    protected void initView() {

    }

    @Override
    protected int initLayout() {
        return R.layout.fragment_b;
    }

    @Override
    protected void lazyLoad() {
        Log.d(TAG,getClass().getSimpleName() + " lazyLoad");
    }

    @Override
    protected void visibleReLoad() {
        Log.d(TAG,getClass().getSimpleName() + " visibleReLoad");
    }

    @Override
    protected void inVisibleRelease() {
        Log.d(TAG,getClass().getSimpleName() + " inVisibleRelease");
    }
}
