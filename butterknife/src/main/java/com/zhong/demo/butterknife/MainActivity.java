package com.zhong.demo.butterknife;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.zhong.demo.butterknife.base.BaseActivity;

import java.util.List;

import butterknife.BindArray;
import butterknife.BindColor;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    //绑定View
    @BindView(com.zhong.demo.butterknife.R.id.bind_btn)
    Button BindBtn;

    //绑定多个View成List
    @BindViews({com.zhong.demo.butterknife.R.id.array_btn_1, com.zhong.demo.butterknife.R.id.array_btn_2, com.zhong.demo.butterknife.R.id.array_btn_3})
    List<Button> btnList;

    //绑定字符串
    @BindString(com.zhong.demo.butterknife.R.string.app_name)
    String appName;
    //绑定数组
    @BindArray(com.zhong.demo.butterknife.R.array.test_array)
    String[] testArray;
    //绑定颜色
    @BindColor(com.zhong.demo.butterknife.R.color.colorAccent)
    int colorAccent;

    @Override
    protected int initLayout() {
        return com.zhong.demo.butterknife.R.layout.activity_main;
    }

    @Override
    protected void create() {
        //onCreate完成

        BindBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BindAdapterActivity.startSelf(MainActivity.this);
            }
        });

        ButterKnife.apply(btnList, action);
        ButterKnife.apply(btnList, setter, "aaaa");
    }


    //设置行为
    ButterKnife.Action<View> action = new ButterKnife.Action<View>() {
        @Override
        public void apply(@NonNull View view, int index) {
            //为view设置行为可点击
            view.setClickable(true);
        }
    };

    //设置属性
    ButterKnife.Setter<Button, String> setter = new ButterKnife.Setter<Button, String>() {
        @Override
        public void set(@NonNull Button view, String value, int index) {
            //为view设置统一属性value
            view.setText(value);
        }
    };

    //绑定点击事件
//    @OnClick(R.id.onclick_btn)
//    protected void onClick(View view){
//        BindFragmentActivity.startSelf(MainActivity.this);
//    }

    @OnClick(com.zhong.demo.butterknife.R.id.onclick_btn)
    protected void onClick() {
        BindFragmentActivity.startSelf(MainActivity.this);
    }



    //给多个View绑定事件
    @OnClick({com.zhong.demo.butterknife.R.id.array_btn_1, com.zhong.demo.butterknife.R.id.array_btn_2, com.zhong.demo.butterknife.R.id.array_btn_3})
    protected void OnArrayClick(View view) {
        switch (view.getId()) {
            case com.zhong.demo.butterknife.R.id.array_btn_1:
//                testGetChannelList();
                break;
            case com.zhong.demo.butterknife.R.id.array_btn_2:
                Toast.makeText(MainActivity.this, "array_btn_2 Button Clicked.", Toast.LENGTH_SHORT).show();
                break;
            case com.zhong.demo.butterknife.R.id.array_btn_3:
                Toast.makeText(MainActivity.this, "array_btn_3 Button Clicked.", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
