package com.zhong.main;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import butterknife.ButterKnife;

/**
 * <pre>
 *     author: Zhong
 *     time  : 2018/12/27
 *     desc  :
 * </pre>
 */
public abstract class BaseActivity extends AppCompatActivity {
    public String TAG = BaseActivity.class.getSimpleName();

    protected Toolbar mToolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(initLayout());

        ButterKnife.bind(this);

        create();
    }

    /**
     * 加载Toolbar，需要子类在布局中添加toolbar布局，所以抛出空指针异常
     * @throws NullPointerException
     */
    protected void loadToolbar() throws NullPointerException{
        mToolbar = findViewById(R.id.tool_bar);

        mToolbar.setTitle(getTitle());
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    protected abstract int initLayout();
    protected abstract void create();
}
