package com.zhong.view_animation;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.zhong.main.BaseActivity;
import com.zhong.main.R;

import butterknife.OnClick;

public class AnimationActivity extends BaseActivity {

//    https://www.cnblogs.com/wondertwo/p/5327586.html
//    http://nightfarmer.github.io/2016/09/12/InterpolatorPreview/
//    https://www.jianshu.com/p/6a6a99309855
//    http://inloop.github.io/interpolator/

    public static void startSelf(Context context){
        Intent intent = new Intent(context,AnimationActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected int initLayout() {
        return R.layout.activity_animation;
    }

    @Override
    protected void create() {
        loadToolbar();
    }

    @OnClick({R.id.view_anim_btn, R.id.popu_anim_btn, R.id.activity_anim_btn,R.id.layout_anim_btn})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.view_anim_btn:
                ViewAnimationActivity.startSelf(AnimationActivity.this);
                break;
            case R.id.popu_anim_btn:
                PopupWindowActivity.startSelf(AnimationActivity.this);
                break;
            case R.id.activity_anim_btn:
                Intent activityAnimIntent = new Intent(AnimationActivity.this, ActivityAnimationActivity.class);
                startActivity(activityAnimIntent);
                //activity 切换动画
                overridePendingTransition(R.anim.activity_enter_alpha_animation,R.anim.activity_out_alpha_animation);
                break;
            case R.id.layout_anim_btn:
                LayoutAnimationActivity.startSelf(AnimationActivity.this);
                break;
        }
    }
}
