package com.zhonge.androiddemo.manager;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.util.Log;

public class MyApplication extends Application {
    public static final String TAG = MyApplication.class.getSimpleName();

    /** Activity监听回调 */
    private ActivityLifecycleCallbacks activityLifecycleCallbacks = new ActivityLifecycleCallbacks() {
        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
            Log.d(TAG, activity.getClass().getSimpleName() + "——onActivityCreated");
        }

        @Override
        public void onActivityStarted(Activity activity) {
            Log.d(TAG, activity.getClass().getSimpleName() + "——onActivityStarted");
        }

        @Override
        public void onActivityResumed(Activity activity) {
            Log.d(TAG, activity.getClass().getSimpleName() + "——onActivityResumed");
        }

        @Override
        public void onActivityPaused(Activity activity) {
            Log.d(TAG, activity.getClass().getSimpleName() + "——onActivityPaused");
        }

        @Override
        public void onActivityStopped(Activity activity) {
            Log.d(TAG, activity.getClass().getSimpleName() + "——onActivityStopped");
        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
            Log.d(TAG, activity.getClass().getSimpleName() + "——onActivitySaveInstanceState");
        }

        @Override
        public void onActivityDestroyed(Activity activity) {
            Log.d(TAG, activity.getClass().getSimpleName() + "——onActivityDestroyed");
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();

        //可注册多次，注册多个生命周期回调
        //应用场景：1、监听某第三方SDK中的Activity启动
        //         2、Activity任务栈管理
        //         3、控制某个Activity被初始化的数量，比如：商城app控制商品详情页的数量，避免限制的初始化Activity
        registerActivityLifecycleCallbacks(activityLifecycleCallbacks);
        //Activity管理
        registerActivityLifecycleCallbacks(ActivityManager.getInstance());
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        //Android API中没有对此方法的过多解释，不过通常此方法会在app退出是被调用（并不是100%）

        unregisterActivityLifecycleCallbacks(activityLifecycleCallbacks);
    }
}
