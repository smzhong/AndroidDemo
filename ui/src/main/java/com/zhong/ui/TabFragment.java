package com.zhong.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * Use the {@link TabFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TabFragment extends Fragment {
    private static final String ARG_FRAGMENT_NAME = "fragmentName";

    private String fragmentName = "";
    private Context context;
    private List<RelativeLayout> items;
    private int focusIndex = -1;

    public TabFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public TabFragment(Context context) {
        this.context = context;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment TabFragment.
     */
    public static TabFragment newInstance(Context context, String param1) {
        TabFragment fragment = new TabFragment(context);
        Bundle args = new Bundle();
        args.putString(ARG_FRAGMENT_NAME, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            fragmentName = getArguments().getString(ARG_FRAGMENT_NAME);
        }

        showToast("onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        showToast("onCreateView");

        View view = inflater.inflate(R.layout.fragment_tab, container, false);
        ((TextView)view.findViewById(R.id.name_tv)).setText(fragmentName);
        items = new ArrayList<>();
        items.add((RelativeLayout) view.findViewById(R.id.item1));
        items.add((RelativeLayout) view.findViewById(R.id.item2));
        items.add((RelativeLayout) view.findViewById(R.id.item3));
        for(RelativeLayout relativeLayout : items){
            relativeLayout.setOnFocusChangeListener(onFocusChangeListener);
        }

        return view;
    }

    View.OnFocusChangeListener onFocusChangeListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            switch (v.getId()){
                case R.id.item1:
                    focusIndex = 0;
                    break;
                case R.id.item2:
                    focusIndex = 1;
                    break;
                case R.id.item3:
                    focusIndex = 2;
                    break;
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        showToast("onResume");
    }

    @Override
    public void onStart() {
        super.onStart();
        showToast("onStart");
    }

    @Override
    public void onPause() {
        super.onPause();
        showToast("onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        showToast("onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        showToast("onDestroy");
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser && items != null && items.size() > 0 && focusIndex > -1){
            if (items.get(focusIndex) != null){
                items.get(focusIndex).requestFocus();
            }
        }
    }

    private Toast toast;

    private void showToast(String msg){
        if(toast == null){
            toast = Toast.makeText(context, msg, Toast.LENGTH_SHORT);
        } else{
            toast.setText(msg);
        }

        Log.d("TabFragment", fragmentName + "---" + msg);

        toast.show();
    }
}
