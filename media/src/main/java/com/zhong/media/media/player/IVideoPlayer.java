package com.zhong.media.media.player;


import android.net.Uri;

import com.zhong.media.media.listener.OnPlayerCallback;

import java.util.List;
import java.util.Map;

public interface IVideoPlayer {
    /**
     * 开始播放
     */
    void startVideo();

    /**
     * 暂定播放
     */
    void pauseVideo();

    /**
     * 停止播放
     */
    void stopVideo();

    /**
     * 释放播放器
     */
    void releasePlayer();

    /**
     * 切换全屏与小窗口
     */
    void fullScreen();

    /**
     * 指定播放器全屏或者小窗口
     * @param isFullScreen
     */
    void fullScreen(boolean isFullScreen);

    /**
     * 设置一组视频播放地址
     * @param videoPathList
     */
    void setVideoPathList(List<String> videoPathList);

    /**
     * 设置视频播放地址
     * @param videoPath
     */
    void setVideoPath(String videoPath);

    /**
     * Sets video URI.
     *
     * @param uri the URI of the video.
     */
    void setVideoURI(Uri uri);

    /**
     * Sets video URI using specific headers.
     *
     * @param uri     the URI of the video.
     * @param headers the headers for the URI request.
     *                Note that the cross domain redirection is allowed by default, but that can be
     *                changed with key/value pairs through the headers parameter with
     *                "android-allow-cross-domain-redirect" as the key and "0" or "1" as the value
     *                to disallow or allow cross domain redirection.
     */
    void setVideoURI(Uri uri, Map<String, String> headers);

    /**
     * 设置回调监听
     * @param onPlayerCallback
     */
    void setOnPlayerCallback(OnPlayerCallback onPlayerCallback);
}
