package com.zhong.mvp.presenter;

import com.zhong.mvp.bean.UserBean;
import com.zhong.mvp.model.ILoginModel;
import com.zhong.mvp.model.LoginModel;
import com.zhong.mvp.view.ILoginView;

/**
 * <pre>
 *     author: Zhong
 *     time  : 2018/12/8
 *     desc  :
 * </pre>
 */
public class LoginPresenter implements ILoginPresenter {

    private ILoginView loginView;
    private ILoginModel loginModel;

    public LoginPresenter(ILoginView loginView){
        this.loginView = loginView;
        loginModel = new LoginModel();
    }

    @Override
    public void getLoginData(UserBean userBean) {
        loginView.showLoading();

        loginModel.doLogin(userBean, new ILoginModel.LoginCallBack() {
            @Override
            public void onSuccess(UserBean data) {
                loginView.showLoginSuccess(data);
            }

            @Override
            public void onFailure(String data) {
                loginView.showFailureMessage(data);
            }

            @Override
            public void onError(String error) {
                loginView.showErrorMessage(error);
            }

            @Override
            public void onComplete() {
                loginView.hideLoading();
            }
        });
    }
}
