package com.zhong.other_animation.transition;

import com.zhong.main.BaseActivity;
import com.zhong.main.R;

public class SharedElementTransitionActivity extends BaseActivity {

    @Override
    protected int initLayout() {
        return R.layout.activity_shared_element_transition;
    }

    @Override
    protected void create() {

    }
}
