package com.zhong.mvp.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.zhong.mvp.R;
import com.zhong.mvp.bean.UserBean;
import com.zhong.mvp.presenter.ILoginPresenter;
import com.zhong.mvp.presenter.LoginPresenter;

public class LoginActivity extends AppCompatActivity implements ILoginView{
    private static final String TAG = LoginActivity.class.getSimpleName();

    private ILoginPresenter loginPresenter;         //login Presenter

    private RadioGroup loginResultRg;               //模拟登录状态的RadioGroup
    private EditText userNameEt;                    //用户名
    private EditText passwordEt;                    //密码

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loginResultRg = findViewById(R.id.login_result_rg);
        userNameEt = findViewById(R.id.user_name_et);
        passwordEt = findViewById(R.id.password_et);

        loginPresenter = new LoginPresenter(this);
    }

    /**
     * 登录事件
     * @param view 事件触发View
     */
    public void login(View view) {
        UserBean userBean = new UserBean();
        userBean.setUserName(userNameEt.getText().toString().trim());
        userBean.setPassword(passwordEt.getText().toString().trim());

        //通过RadioButton的选中状态模拟不同的登录状态
        switch (loginResultRg.getCheckedRadioButtonId()){
            case R.id.success_rb:
                userBean.setLoginResultType("1");
                break;
            case R.id.failure_rb:
                userBean.setLoginResultType("2");
                break;
            case R.id.error_rb:
                userBean.setLoginResultType("3");
                break;
        }
        loginPresenter.getLoginData(userBean);
    }

    @Override
    public void showLoading() {
        Log.d(TAG, "showLoading");

        Toast.makeText(LoginActivity.this, "showLoading", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void hideLoading() {
        Log.d(TAG, "hideLoading");
        Toast.makeText(LoginActivity.this, "hideLoading", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoginSuccess(UserBean userBean) {
        Log.d(TAG, "showLoginSuccess user Information " + userBean.toString());
        Toast.makeText(LoginActivity.this, "showLoginSuccess userName=" + userBean.toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showFailureMessage(String message) {
        Log.d(TAG, "showFailureMessage message= " + message);
        Toast.makeText(LoginActivity.this, "showFailureMessage msg=" + message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showErrorMessage(String message) {
        Log.d(TAG, "showErrorMessage message=" + message);
        Toast.makeText(LoginActivity.this, "showErrorMessage msg=" + message, Toast.LENGTH_SHORT).show();
    }
}
