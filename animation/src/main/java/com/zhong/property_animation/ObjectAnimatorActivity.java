package com.zhong.property_animation;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.zhong.main.BaseActivity;
import com.zhong.main.R;

import butterknife.BindView;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

/**
 * 属性动画——ObjectAnimator
 */
public class ObjectAnimatorActivity extends BaseActivity {

    @BindView(R.id.object_anim_iv)
    ImageView imageView;

    private AnimatorSet animatorSet;

    public static void startSelf(Context context) {
        Intent intent = new Intent(context, ObjectAnimatorActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected int initLayout() {
        return R.layout.activity_object_animator;
    }

    @Override
    protected void create() {

    }

    @OnClick({R.id.start_btn, R.id.stop_btn})
    protected void onClick(View view) {
        switch (view.getId()) {
            case R.id.start_btn:
                if(animatorSet == null){
                    Log.e(TAG,"please check animator first");
                    return;
                }

                animatorSet.start();
                break;
            case R.id.stop_btn:
                if(animatorSet == null){
                    Log.e(TAG,"please check animator first");
                    return;
                }

                animatorSet.cancel();
                break;
        }
    }

    @OnCheckedChanged({R.id.alpha_cb, R.id.rotation_cb, R.id.rotation_x_cb, R.id.rotation_y_cb,
        R.id.translation_x_cb, R.id.translation_y_cb, R.id.scale_x_cb, R.id.scale_y_cb,
        R.id.rgb_cb, R.id.set_cb, R.id.xml_color_cb,R.id.xml_cb, R.id.value_holder_cb})
    protected void onCheckChanged(CheckBox view, boolean isChecked) {
        if(isChecked){
            initAnimatorSet();
            switch (view.getId()) {
                case R.id.alpha_cb:
                    animatorSet.play(getAlphaAnimator());
                    break;
                case R.id.rotation_cb:
                    //设置View的rotation和scale两种行为的坐标系原点坐标
                    //以View左上角为参照
                    imageView.setPivotX(50);
                    imageView.setPivotY(50);

                    animatorSet.play(getRotationAnimator());
                    break;
                case R.id.rotation_x_cb:
                    animatorSet.play(getRotationXAnimator());
                    break;
                case R.id.rotation_y_cb:
                    animatorSet.play(getRotationYAnimator());
                    break;
                case R.id.translation_x_cb:
                    animatorSet.play(getTranslationXAnimator());
                    break;
                case R.id.translation_y_cb:
                    animatorSet.play(getTranslationYAnimator());
                    break;
                case R.id.scale_x_cb:
                    animatorSet.play(getScaleXAnimator());
                    break;
                case R.id.scale_y_cb:
                    animatorSet.play(getScaleYAnimator());
                    break;
                case R.id.rgb_cb:
                    animatorSet.play(getColorAnimator());
                    break;
                case R.id.xml_color_cb:
                    animatorSet.play(AnimatorInflater.loadAnimator(ObjectAnimatorActivity.this, R.animator.color_animator));
                    animatorSet.setTarget(imageView);
                    break;
                case R.id.set_cb:
                    animatorSet.play(getAlphaAnimator()).after(getRotationAnimator()).with(getTranslationYAnimator());
                    break;
                case R.id.xml_cb:
                    animatorSet.play(loadAnimatorFromXml());
                    animatorSet.setTarget(imageView);

                    break;
                case R.id.value_holder_cb:
                    PropertyValuesHolder valuesHolder1 = PropertyValuesHolder.ofFloat("translationX", 0, 200, 100);
                    PropertyValuesHolder valuesHolder2 = PropertyValuesHolder.ofFloat("translationX", 0, 200, 100);
                    ObjectAnimator objectAnimator = ObjectAnimator.ofPropertyValuesHolder(imageView,valuesHolder1,valuesHolder2);
                    objectAnimator.setDuration(2000);
                    objectAnimator.start();

//                    CustomObject customObject = new CustomObject();
//                    ObjectAnimator customAnimator = ObjectAnimator.ofInt(customObject,"customParam", 1, 1000);
//                    customAnimator.setDuration(10000);
//                    customAnimator.start();
                    break;
            }
        } else{
            //去掉
        }
    }

    /**
     * 初始化AnimatorSet
     */
    private void initAnimatorSet() {
        if (animatorSet == null) {
            animatorSet = new AnimatorSet();
            animatorSet.setDuration(3000);  //播放时间
            animatorSet.setTarget(imageView);
            animatorSet.setStartDelay(0);

            //监听动画执行
            animatorSet.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationStart(Animator animation, boolean isReverse) {
                    Log.d(TAG, "动画开始");
                }

                @Override
                public void onAnimationEnd(Animator animation, boolean isReverse) {
                    Log.d(TAG, "动画结束");
                }
            });

        }
    }

    /**
     * 从xml加载animator
     * @return ObjectAnimator
     */
    private Animator loadAnimatorFromXml(){
        return AnimatorInflater.loadAnimator(ObjectAnimatorActivity.this, R.animator.translation_set_animator);
    }

    /**
     * 颜色Animator
     * @return ObjectAnimator
     */
    private ObjectAnimator getColorAnimator(){
        ObjectAnimator colorAnimator = ObjectAnimator.ofArgb(imageView, "backgroundColor", 0xffff0000, 0xff0000ff);
        colorAnimator.setRepeatCount(1);  //重复播放次数
        colorAnimator.setDuration(3000);  //播放时间
        colorAnimator.setRepeatMode(ValueAnimator.REVERSE);   //重复播放模式，REVERSE倒播,RESTART重播
        colorAnimator.setStartDelay(0);   //开始播放前的延时时间
        return colorAnimator;
    }

    /**
     * 透明度
     * @return
     */
    private ObjectAnimator getAlphaAnimator() {
        ObjectAnimator alphaAnimator = ObjectAnimator.ofFloat(imageView,
                "alpha", 1, 0);
        alphaAnimator.setRepeatCount(1);  //重复播放次数
        alphaAnimator.setDuration(3000);  //播放时间
        alphaAnimator.setRepeatMode(ValueAnimator.REVERSE);   //重复播放模式，REVERSE倒播,RESTART重播
        alphaAnimator.setStartDelay(0);   //开始播放前的延时时间

        //设置监听，监听animator执行状态
        alphaAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        //设置监听，监听animator执行过程的值变化情况
        alphaAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float currentAlpha = (float) animation.getAnimatedValue();
                Log.d(TAG,"当前透明度 alpha = " + currentAlpha);
            }
        });

        alphaAnimator.start();

        return alphaAnimator;
    }

    /**
     * 旋转
     * @return
     */
    private ObjectAnimator getRotationAnimator(){
        ObjectAnimator rotationAnimator = ObjectAnimator.ofFloat(imageView,"rotation",0, 360);
        rotationAnimator.setRepeatCount(1);  //重复播放次数
        rotationAnimator.setDuration(3000);  //播放时间
        rotationAnimator.setRepeatMode(ValueAnimator.REVERSE);   //重复播放模式，REVERSE倒播,RESTART重播
        rotationAnimator.setStartDelay(0);   //开始播放前的延时时间

        return rotationAnimator;
    }

    /**
     * 旋转X
     * @return
     */
    private ObjectAnimator getRotationXAnimator(){
        ObjectAnimator rotationXAnimator = ObjectAnimator.ofFloat(imageView,"rotationX",0, 360);
        rotationXAnimator.setRepeatCount(1);  //重复播放次数
        rotationXAnimator.setDuration(3000);  //播放时间
        rotationXAnimator.setRepeatMode(ValueAnimator.REVERSE);   //重复播放模式，REVERSE倒播,RESTART重播
        rotationXAnimator.setStartDelay(0);   //开始播放前的延时时间

        return rotationXAnimator;
    }

    /**
     * 旋转Y
     * @return
     */
    private ObjectAnimator getRotationYAnimator(){
        ObjectAnimator rotationYAnimator = ObjectAnimator.ofFloat(imageView,"rotationY",0, 360);
        rotationYAnimator.setRepeatCount(1);  //重复播放次数
        rotationYAnimator.setDuration(3000);  //播放时间
        rotationYAnimator.setRepeatMode(ValueAnimator.REVERSE);   //重复播放模式，REVERSE倒播,RESTART重播
        rotationYAnimator.setStartDelay(0);   //开始播放前的延时时间

        return rotationYAnimator;
    }

    private ObjectAnimator getTranslationXAnimator(){
        ObjectAnimator translationXAnimator = ObjectAnimator.ofFloat(imageView,"translationX",0,200);
        translationXAnimator.setRepeatCount(1);  //重复播放次数
        translationXAnimator.setDuration(3000);  //播放时间
        translationXAnimator.setRepeatMode(ValueAnimator.REVERSE);   //重复播放模式，REVERSE倒播,RESTART重播
        translationXAnimator.setStartDelay(0);   //开始播放前的延时时间

        return translationXAnimator;
    }

    private ObjectAnimator getTranslationYAnimator(){
        ObjectAnimator translationYAnimator = ObjectAnimator.ofFloat(imageView,"translationY",0,200);
        translationYAnimator.setRepeatCount(1);  //重复播放次数
        translationYAnimator.setDuration(3000);  //播放时间
        translationYAnimator.setRepeatMode(ValueAnimator.REVERSE);   //重复播放模式，REVERSE倒播,RESTART重播
        translationYAnimator.setStartDelay(0);   //开始播放前的延时时间

        return translationYAnimator;
    }

    private ObjectAnimator getScaleXAnimator(){
        ObjectAnimator scaleXAnimator = ObjectAnimator.ofFloat(imageView,"scaleX",1f,1.5f);
        scaleXAnimator.setRepeatCount(1);  //重复播放次数
        scaleXAnimator.setDuration(3000);  //播放时间
        scaleXAnimator.setRepeatMode(ValueAnimator.REVERSE);   //重复播放模式，REVERSE倒播,RESTART重播
        scaleXAnimator.setStartDelay(0);   //开始播放前的延时时间

        return scaleXAnimator;
    }

    private ObjectAnimator getScaleYAnimator(){
        ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(imageView,"scaleY",1f,1.5f);
        scaleYAnimator.setRepeatCount(1);  //重复播放次数
        scaleYAnimator.setDuration(3000);  //播放时间
        scaleYAnimator.setRepeatMode(ValueAnimator.REVERSE);   //重复播放模式，REVERSE倒播,RESTART重播
        scaleYAnimator.setStartDelay(0);   //开始播放前的延时时间

        return scaleYAnimator;
    }
}
