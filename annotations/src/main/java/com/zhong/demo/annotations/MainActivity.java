package com.zhong.demo.annotations;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.AnimatorRes;
import android.support.annotation.AnyRes;
import android.support.annotation.AnyThread;
import android.support.annotation.ArrayRes;
import android.support.annotation.BinderThread;
import android.support.annotation.BoolRes;
import android.support.annotation.CheckResult;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.FloatRange;
import android.support.annotation.FontRes;
import android.support.annotation.IntDef;
import android.support.annotation.IntRange;
import android.support.annotation.IntegerRes;
import android.support.annotation.InterpolatorRes;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresPermission;
import android.support.annotation.Size;
import android.support.annotation.StringDef;
import android.support.annotation.StringRes;
import android.support.annotation.UiThread;
import android.support.annotation.WorkerThread;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        int c = (int) (Math.random() * 3000);
//        Logger.d("setAlpha int for result " + setAlpha(c));

        try {
            setWallpaper();
        } catch (IOException e) {
            e.printStackTrace();
        }

        check(1, 1);
    }


    /*******************************
    //Nullness注解
    @Nullable       //检查给定变量、参数或返回值可为空
    @NonNull        //检查给定变量、参数或返回值不可为空

    private void testNullness(@NonNull Context context){}

    //资源注解
    @StringRes      //字符串资源
    @IntegerRes     //整型资源
    @ArrayRes       //数组型资源
    @BoolRes        //布尔型资源
    //...
    @FontRes        //字体资源
    @InterpolatorRes
    @ColorRes       //颜色资源
    @DrawableRes    //Drawable资源
    @AnimatorRes    //动画资源
    @AnyRes         //任意资源

    public void setColor(@ColorRes int color){
        //指定参数为Color型资源，若传递R.string型资源，则会提示错误
    }

    //线程注解
    @MainThread     //主线程
    @UiThread       //UI线程
    @WorkerThread   //工作线程
    @BinderThread   //
    @AnyThread      //任意线程

    //范围注解
    @IntRange       //int范围
    @FloatRange     //float范围
    @Size           //检查集合、数组和字符串的长度，min=1，max=3，@size(3)

    private int setAlpha(@IntRange(from = 0, to = 255) int alpha){return alpha;}
    private float setAlpha(@FloatRange(from = 0.0,to = 1.0) float alpha){return alpha;}
    private void setSize(@Size(3) int[] array){}            //参数长度为3
    private void setSize(@Size(min = 1) float[] array){}    //参数至少有一个元素
    private void setSize(@Size(max = 3) String[] array){}   //参数最多有三个元素
     *************************/

    @RequiresPermission     //权限注解,
                            //anyOf 检查一组中的任意一个权限
                            //allOf 检查一组权限



    @RequiresPermission(Manifest.permission.SET_WALLPAPER)
    public void setWallpaper() throws IOException{}

    @RequiresPermission(allOf = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE})
    public static final void copyFile(String dest, String source) {}




    @CheckResult(suggest = "check(int pid)")
    public int check(int pid, int uid){return 0;}

    public void check(int pid){}

    //TypeDef注解
    @IntDef
    @StringDef

    //定义了两个常量
    public static final int REQUEST_TYPE_AQIYI = 0;
    public static final int REQUEST_TYPE_LOCAL = 1;

    //定义一个RequestType声明可接收的常量
    @IntDef({REQUEST_TYPE_AQIYI,REQUEST_TYPE_LOCAL})
    public @interface RequestType{}

    @RequestType
    public int getRequestType(){
        //返回值只能是REQUEST_TYPE_AQIYI和REQUEST_TYPE_LOCAL的其中一个
        //若不是则会生成警告

        return REQUEST_TYPE_AQIYI;
    }

    public void requestMovieData(@RequestType int type){
        //返回值只能是REQUEST_TYPE_AQIYI和REQUEST_TYPE_LOCAL的其中一个
        //若不是则会生成警告
    }
}
