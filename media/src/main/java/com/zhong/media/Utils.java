package com.zhong.media;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.IOException;

/**
 * <pre>
 *     author: Zhong
 *     time  : 2020/3/11
 *     desc  :
 * </pre>
 */
public class Utils {
    public static File getVideoCacheDir(Context context) {
        File file = new File(context.getFilesDir(), "video-cache");
        Log.d("TAG", file.getAbsolutePath());
        return file;
    }

    public static void cleanVideoCacheDir(Context context) throws IOException {
        File videoCacheDir = getVideoCacheDir(context);
        cleanDirectory(videoCacheDir);
    }

    private static void cleanDirectory(File file) throws IOException {
        if (!file.exists()) {
            return;
        }
        File[] contentFiles = file.listFiles();
        if (contentFiles != null) {
            for (File contentFile : contentFiles) {
                delete(contentFile);
            }
        }
    }

    private static void delete(File file) throws IOException {
        if (file.isFile() && file.exists()) {
            deleteOrThrow(file);
        } else {
            cleanDirectory(file);
            deleteOrThrow(file);
        }
    }

    private static void deleteOrThrow(File file) throws IOException {
        if (file.exists()) {
            boolean isDeleted = file.delete();
            if (!isDeleted) {
                throw new IOException(String.format("File %s can't be deleted", file.getAbsolutePath()));
            }
        }
    }
}
