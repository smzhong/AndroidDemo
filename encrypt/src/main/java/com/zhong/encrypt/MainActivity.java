package com.zhong.encrypt;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private MyRecyclerViewAdapter adapter;
    private List<String> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initData();

        mRecyclerView = findViewById(R.id.recycler_view);
        mLayoutManager = new LinearLayoutManager(MainActivity.this);
        ((LinearLayoutManager)mLayoutManager).setOrientation(LinearLayoutManager.VERTICAL);
        adapter = new MyRecyclerViewAdapter();
        adapter.setData(data);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(adapter);

        mRecyclerView.addItemDecoration(new MyItemDecoration(Color.GREEN, 20,50));

        mRecyclerView.addOnItemTouchListener(new MyOnItemTouchListener(MainActivity.this, mRecyclerView, new OnRecyclerItemClick(){

            @Override
            public void onClick(View view, int position) {
                Intent intent = new Intent();
                switch (position){
                    case 0:
                        intent.setClass(MainActivity.this, MD5Activity.class);
                        break;
                    case 1:
                        intent.setClass(MainActivity.this, AESActivity.class);
                        break;
                    case 2:
                        intent.setClass(MainActivity.this, RSAActivity.class);
                        break;
                }

                if(intent.getComponent() != null){
                    MainActivity.this.startActivity(intent);
                }
            }

            @Override
            public void onLongClick(View view, int position) {
                Toast.makeText(MainActivity.this, "onLongClick", Toast.LENGTH_SHORT).show();
            }
        }));
    }

    private void initData(){
        data = new ArrayList<>();
        data.add("MD5");
        data.add("AES");
        data.add("RSA");
    }

    class MyOnItemTouchListener implements RecyclerView.OnItemTouchListener{
        private GestureDetector mGestureDetector;
        private Context mContext;
        private RecyclerView mRecyclerView;
        private OnRecyclerItemClick mOnRecyclerItemClick;

        public MyOnItemTouchListener(Context mContext, final RecyclerView mRecyclerView, final OnRecyclerItemClick mOnRecyclerItemClick){
            if(null == mContext || null == mRecyclerView){
                throw new IllegalArgumentException("非法参数");
            }

            this.mContext = mContext;
            this.mRecyclerView = mRecyclerView;
            this.mOnRecyclerItemClick = mOnRecyclerItemClick;
            mGestureDetector = new GestureDetector(mContext, new GestureDetector.SimpleOnGestureListener(){
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    if(null != mOnRecyclerItemClick){
                        View view = mRecyclerView.findChildViewUnder(e.getX(), e.getY());
                        int position = mRecyclerView.getChildAdapterPosition(view);
                        mOnRecyclerItemClick.onClick(view, position);

                        return true;
                    }
                    return super.onSingleTapUp(e);
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    super.onLongPress(e);
                    if(null != mOnRecyclerItemClick){
                        View view = mRecyclerView.findChildViewUnder(e.getX(), e.getY());
                        int position = mRecyclerView.getChildAdapterPosition(view);
                        mOnRecyclerItemClick.onLongClick(view, position);
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            return mGestureDetector.onTouchEvent(e);
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }


    }

    public interface OnRecyclerItemClick {
        void onClick(View view, int position);
        void onLongClick(View view, int position);
    }

    class MyRecyclerViewAdapter extends RecyclerView.Adapter{
        private List<String> data = new ArrayList<>();

        public void addData(List<String> data){
            if(null != data){
                this.data.addAll(data);

                notifyDataSetChanged();
            }
        }

        public void setData(List<String> data){
            this.data.clear();
            addData(data);
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler, parent, false);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            ((MyViewHolder)holder).tvName.setText(data.get(position));
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        class MyViewHolder extends RecyclerView.ViewHolder{
            TextView tvName;

            public MyViewHolder(View itemView) {
                super(itemView);
                tvName = itemView.findViewById(R.id.tv_name);
            }
        }
    }

    class MyItemDecoration extends RecyclerView.ItemDecoration{
        private int dividerHeight = 0;
        private int dividerWeight = 0;

        private int dividerColor = Color.TRANSPARENT;

        private Paint paint;

        public MyItemDecoration() {
            this(0,0);
        }

        public MyItemDecoration(int dividerColor, int dividerHeight){
            this(dividerColor, dividerHeight, 0);
        }

        public MyItemDecoration(int dividerColor, int dividerHeight, int dividerWeight){
            this.dividerColor = dividerColor;
            this.dividerHeight = dividerHeight;
            this.dividerWeight = dividerWeight;

            paint = new Paint();
            paint.setColor(dividerColor);
        }

        public int getDividerHeight() {
            return dividerHeight;
        }

        public void setDividerHeight(int dividerHeight) {
            this.dividerHeight = dividerHeight;
        }

        public int getDividerWeight() {
            return dividerWeight;
        }

        public void setDividerWeight(int dividerWeight) {
            this.dividerWeight = dividerWeight;
        }

        public int getDividerColor() {
            return dividerColor;
        }

        public void setDividerColor(int dividerColor) {
            this.dividerColor = dividerColor;
        }

        @Override
        public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
            super.onDraw(c, parent, state);
            for(int i = 0; i < parent.getChildCount(); i++){
//                View childView = parent.getChildAt(i);
//                int leftH = childView.getLeft();
//                int rightH = childView.getRight() + 20;
//                int topH = childView.getBottom();
//                int bottomH = topH + 20;
//
//                c.drawRect(leftH, topH, rightH, bottomH, paint);
            }

        }

        @Override
        public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
            super.onDrawOver(c, parent, state);
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);

            int topDividerHeight = 0;
            int leftDividerHeight = 0;
            int rightDividerHeight = 0;
            int bottomDividerHeight = 0;

            RecyclerView.LayoutManager layoutManager = parent.getLayoutManager();
            if(layoutManager instanceof LinearLayoutManager){
                if (((LinearLayoutManager) layoutManager).getOrientation() == LinearLayoutManager.VERTICAL) {
                    //垂直布局
                    if(parent.getChildItemId(view) == parent.getChildCount() - 1){
                        bottomDividerHeight = 0;
                    } else{
                        bottomDividerHeight = dividerHeight;
                    }
                } else {
                    //水平布局
                    if(parent.getChildItemId(view) == parent.getChildCount() - 1){
                        rightDividerHeight = 0;
                    } else{
                        rightDividerHeight = dividerHeight;
                    }
                }
            } else if(layoutManager instanceof GridLayoutManager){
                if(((GridLayoutManager) layoutManager).getOrientation() == GridLayoutManager.VERTICAL){
                    //垂直布局
                    bottomDividerHeight = dividerHeight;
//                    rightDividerHeight = dividerWeight / 2;
                    leftDividerHeight = dividerWeight / 2;
                } else{
                    //水平布局
                    topDividerHeight = dividerHeight / 2;
                    bottomDividerHeight = dividerHeight / 2;
//                    rightDividerHeight = dividerWeight / 2;
                    leftDividerHeight = dividerWeight / 2;
                }
            } else if(layoutManager instanceof StaggeredGridLayoutManager){
                if(((StaggeredGridLayoutManager) layoutManager).getOrientation() == StaggeredGridLayoutManager.VERTICAL){
                    //垂直布局
                } else{
                    //水平布局
                }
            }

            outRect.set(leftDividerHeight,topDividerHeight,rightDividerHeight,bottomDividerHeight);
        }
    }
}
