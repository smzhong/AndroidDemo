package com.zhong.other_animation.transition;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.transition.Explode;
import android.transition.Fade;
import android.transition.Slide;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.zhong.main.BaseActivity;
import com.zhong.main.R;

import butterknife.BindView;
import butterknife.OnClick;

public class ActivityTransitionsActivity extends BaseActivity {

    @BindView(R.id.tool_bar)
    Toolbar toolbar;
    @BindView(R.id.explode_code_btn)
    Button explodeCodeBtn;
    @BindView(R.id.explode_xml_btn)
    Button explodeXmlBtn;
    @BindView(R.id.slide_code_btn)
    Button slideCodeBtn;
    @BindView(R.id.slide_xml_btn)
    Button slideXmlBtn;
    @BindView(R.id.fade_code_btn)
    Button fadeCodeBtn;
    @BindView(R.id.fade_xml_btn)
    Button fadeXmlBtn;

    public static void startSelf(Context context){
        Intent intent = new Intent(context, ActivityTransitionsActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected int initLayout() {
        return R.layout.activity_transitions;
    }

    @Override
    protected void create() {
        initToolBar();
    }

    private void initToolBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @OnClick({R.id.explode_code_btn,R.id.explode_xml_btn, R.id.slide_code_btn, R.id.slide_xml_btn, R.id.fade_code_btn, R.id.fade_xml_btn})
    protected void onClick(View view){
        Transition transition = null;
        switch (view.getId()){
            case R.id.explode_code_btn:
                transition = new Explode().setDuration(2000);
                break;
            case R.id.explode_xml_btn:
                transition = TransitionInflater.from(this).inflateTransition(R.transition.activity_explode);
                break;
            case R.id.slide_code_btn:
                transition = new Slide().setDuration(2000);
                break;
            case R.id.slide_xml_btn:
                transition = TransitionInflater.from(this).inflateTransition(R.transition.activity_slide);
                break;
            case R.id.fade_code_btn:
                transition = new Fade().setDuration(2000);
                break;
            case R.id.fade_xml_btn:
                transition = TransitionInflater.from(this).inflateTransition(R.transition.activity_fade);
                break;
        }

        if(transition != null){
            getWindow().setExitTransition(transition);
        }

        ActivityTransitionsToActivity.startSelf(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
