package com.zhong.encrypt;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


import com.zhong.encrypt.Utils.AESUtil;
import com.zhong.encrypt.Utils.HexUtil;

import javax.crypto.SecretKey;

public class AESActivity extends AppCompatActivity {
    private static final String TAG = AESActivity.class.getSimpleName();

    private TextView logTextView;
    private EditText seedEt;            //种子
    private TextView keyTv;             //key
    private EditText textEt;
    private TextView secretTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aes);

        initView();
    }

    private void initView() {
        logTextView = findViewById(R.id.log_tv);
        seedEt = findViewById(R.id.seed_et);
        keyTv = findViewById(R.id.key_tv);
        textEt = findViewById(R.id.text_et);
        secretTv = findViewById(R.id.secret_tv);

        findViewById(R.id.secret_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String seedStr = seedEt.getText().toString().trim();
                String textStr = textEt.getText().toString().trim();

                if (TextUtils.isEmpty(seedStr) || TextUtils.isEmpty(textStr)) {
                    return;
                }

                try {
                    SecretKey secretKey = AESUtil.generateKey(seedStr);
                    byte[] keyEncoded = secretKey.getEncoded();
                    String secretKeyStr = HexUtil.encode(keyEncoded);
                    byte[] secretKeyByte2 = HexUtil.decode(secretKeyStr);
//                    byte[] keyEncoded = HexUtil.decode("78dfccde0c0c8e63b0f094068e953cfc");

                    byte[] secretByte = AESUtil.encrypt(textStr, keyEncoded);
                    String secretStr = AESUtil.encryptToHex(textStr, keyEncoded);

                    byte[] decryptByte = AESUtil.decrypt(secretByte, keyEncoded);
                    String decryptStr = AESUtil.decryptToStr(secretByte, keyEncoded);

                    keyTv.setText(secretKeyStr);
                    secretTv.setText(secretStr);

                    showLog("种子：" + seedStr, false);
                    showLog("秘钥：" + secretKeyStr, true);
                    showLog("明文：" + textStr, true);
                    showLog("密文：" + secretStr, true);
                    showLog("解密：" + decryptStr, true);


                } catch (Exception e) {
                    e.printStackTrace();
                    showLog("秘钥创建失败", false);
                }
            }
        });
    }


    private void showLog(String log, boolean isAppend){
        Log.d(TAG, log);
        if(isAppend){
            log = logTextView.getText().toString() + log + "\n";
        } else{
            log += "\n";
        }

        logTextView.setText(log);
    }
}
