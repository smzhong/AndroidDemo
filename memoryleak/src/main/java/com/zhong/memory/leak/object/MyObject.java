package com.zhong.memory.leak.object;

/**
 * <pre>
 *     author: Zhong
 *     time  : 2019/5/15
 *     desc  :
 * </pre>
 */
public class MyObject {
    private String name;

    public MyObject(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
