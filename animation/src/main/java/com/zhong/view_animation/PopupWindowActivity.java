package com.zhong.view_animation;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.PopupWindow;

import com.zhong.main.BaseActivity;
import com.zhong.main.R;

import butterknife.OnClick;

public class PopupWindowActivity extends BaseActivity {
    private PopupWindow mPopupWindow;

    private Dialog dialog;

    public static void startSelf(Context context){
        Intent intent = new Intent(context, PopupWindowActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected int initLayout() {
        return R.layout.activity_popup_window;
    }

    @Override
    protected void create() {
        loadToolbar();
    }

    @OnClick({R.id.show_popup_btn, R.id.show_dialog_btn})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.show_popup_btn:
                showImgPopupWindow(view);
                break;
            case R.id.show_dialog_btn:
                showDialog();
                break;
        }
    }

    /**
     * Show PopupWindow
     * @param anchor
     */
    private void showImgPopupWindow(View anchor) {
        if (mPopupWindow == null) {
            ImageView view = new ImageView(this);
            view.setBackgroundColor(Color.parseColor("#FF989898"));
            view.setImageDrawable(getDrawable(R.mipmap.ic_launcher));

            mPopupWindow = new PopupWindow(view, anchor.getMeasuredWidth(), anchor.getMeasuredWidth());
            mPopupWindow.setAnimationStyle(R.style.PopupWindowAnimationStyle);
        }
        if (mPopupWindow.isShowing()) {
            mPopupWindow.dismiss();
        } else {
            mPopupWindow.showAsDropDown(anchor);
        }
    }

    private void showDialog(){
        if(dialog == null){
            AlertDialog.Builder builder = new AlertDialog.Builder(PopupWindowActivity.this);
            builder.setTitle("Dialog");
            builder.setMessage("This is a Dialog");
            builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            builder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });

            dialog = builder.create();
            dialog.getWindow().setWindowAnimations(R.style.PopupWindowAnimationStyle);
        }

        dialog.show();
    }
}
