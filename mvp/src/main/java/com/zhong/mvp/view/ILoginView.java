package com.zhong.mvp.view;

import com.zhong.mvp.bean.UserBean;

/**
 * <pre>
 *     author: Zhong
 *     time  : 2018/12/7
 *     desc  :
 * </pre>
 */
public interface ILoginView {
    void showLoading();
    void hideLoading();

    /**
     * 登录成功
     * @param userBean 用户类
     */
    void showLoginSuccess(UserBean userBean);

    /**
     * 显示登录失败信息
     * @param message 失败信息
     */
    void showFailureMessage(String message);

    /**
     * 显示登录错误信息
     * @param message 错误信息
     */
    void showErrorMessage(String message);
}
