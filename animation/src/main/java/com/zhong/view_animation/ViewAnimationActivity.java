package com.zhong.view_animation;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.CycleInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.zhong.main.BaseActivity;
import com.zhong.main.R;

import butterknife.BindView;
import butterknife.OnClick;

public class ViewAnimationActivity extends BaseActivity {
    /**
     * animation加载方式，从xml或者代码
     */
    private enum LoadAnimationFromType{
        XML, CODE
    }

    @BindView(R.id.anim_rg)
    RadioGroup mAnimationRadioGroup;
    @BindView(R.id.start_btn)
    Button mStartAnimaBtn;
    @BindView(R.id.stop_btn)
    Button mStopAnimaBtn;
    @BindView(R.id.iv)
    ImageView mImageView;
    @BindView(R.id.load_type_rg)
    RadioGroup mLoadTypeRadioGroup;
    @BindView(R.id.interpolator_tv)
    TextView mInterpolatorTv;

    private Interpolator mInterpolator = null;

    //animation加载方式，从xml或者代码
    private LoadAnimationFromType loadAnimationFromType = LoadAnimationFromType.CODE;

    public static void startSelf(Context context){
        Intent intent = new Intent(context, ViewAnimationActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected int initLayout() {
        return R.layout.activity_view_animation;
    }

    @Override
    protected void create() {
        loadToolbar();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.view_animation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        String interpolatorName = "";
        switch (id){
            case R.id.clear:
                mInterpolator = null;
                break;
            case R.id.AccelerateDecelerateInterpolator:
                //在动画开始与结束的地方速率改变比较慢，在中间的时候加速
                interpolatorName = "AccelerateDecelerateInterpolator";
                mInterpolator = new AccelerateDecelerateInterpolator();
                break;
            case R.id.AccelerateInterpolator:
                //在动画开始的地方速率改变比较慢，然后开始加速
                interpolatorName = "AccelerateInterpolator";
                mInterpolator = new AccelerateInterpolator();
                break;
            case R.id.AnticipateInterpolator:
                //开始的时候向后然后向前甩
                interpolatorName = "AnticipateInterpolator";
                mInterpolator = new AnticipateInterpolator();
                break;
            case R.id.AnticipateOvershootInterpolator:
                //开始的时候向后然后向前甩一定值后返回最后的值
                interpolatorName = "AnticipateOvershootInterpolator";
                mInterpolator = new AnticipateOvershootInterpolator();
                break;
            case R.id.BounceInterpolator:
                //动画结束的时候弹起
                interpolatorName = "BounceInterpolator";
                mInterpolator = new BounceInterpolator();
                break;
            case R.id.CycleInterpolator:
                //动画循环播放特定的次数，速率改变沿着正弦曲线
                interpolatorName = "CycleInterpolator";
                mInterpolator = new CycleInterpolator(7);
                break;
            case R.id.DecelerateInterpolator:
                //在动画开始的地方快然后慢
                interpolatorName = "DecelerateInterpolator";
                mInterpolator = new DecelerateInterpolator();
                break;
            case R.id.LinearInterpolator:
                //以常量速率改变
                interpolatorName = "LinearInterpolator";
                mInterpolator = new LinearInterpolator();
                break;
            case R.id.OvershootInterpolator:
                //向前甩一定值后再回到原来位置
                interpolatorName = "OvershootInterpolator";
                mInterpolator = new OvershootInterpolator();
                break;
        }

        mInterpolatorTv.setText(interpolatorName);
        return super.onOptionsItemSelected(item);
    }


    @OnClick({R.id.stop_btn,R.id.start_btn})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.start_btn:
                switch (mLoadTypeRadioGroup.getCheckedRadioButtonId()){
                    case R.id.load_type_code_rb:
                        loadAnimationFromType = LoadAnimationFromType.CODE;
                        break;
                    case R.id.load_type_xml_rb:
                        loadAnimationFromType = LoadAnimationFromType.XML;
                        break;
                }

                Animation animation = getTransAnimation(loadAnimationFromType);
                switch (mAnimationRadioGroup.getCheckedRadioButtonId()){
                    case R.id.trans_rb:
                        animation = getTransAnimation(loadAnimationFromType);
                        break;
                    case R.id.scale_rb:
                        animation = getScaleAnimation(loadAnimationFromType);
                        break;
                    case R.id.rotate_rb:
                        animation = getRotateAnimation(loadAnimationFromType);
                        break;
                    case R.id.alpha_rb:
                        animation = getAlphaAnimation(loadAnimationFromType);
                        break;
                    case R.id.anim_set_rb:
                        animation = getAnimationSet(loadAnimationFromType);
                        break;
                }

                if(mInterpolator != null){
                    animation.setInterpolator(mInterpolator);
                }

                animation.setDuration(500);
                doAnimation(animation);
                break;
            case R.id.stop_btn:
                cancelAnimation();
                break;
        }
    }

    /**
     * 开始动画
     * @param animation
     */
    private void doAnimation(Animation animation){
        cancelAnimation();

        //动画监听
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                //动画开始
                Log.d(TAG, "onAnimationStart");
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                //动画结束
                Log.d(TAG, "onAnimationEnd");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                //动画重复执行时触发
                Log.d(TAG, "onAnimationRepeat");
            }
        });

        //开始动画
        mImageView.startAnimation(animation);
    }

    /**
     * 清除动画
     */
    private void cancelAnimation(){
        Animation animation = mImageView.getAnimation();

        if(animation != null && animation.hasStarted() && !animation.hasEnded()){
            animation.cancel();
            mImageView.clearAnimation();
        }
    }

    /**
     * 平移动画
     * @return 动画对象
     */
    private Animation getTransAnimation(LoadAnimationFromType loadAnimationFromType){
//        TranslateAnimation(int fromXType, float fromXValue, int toXType, float toXValue,
//        int fromYType, float fromYValue, int toYType, float toYValue)

        //Type 指定动画移动类型，ABSOLUTE——绝对坐标值
        //                      RELATIVE_TO_SELF——自身尺寸乘以value
        //                      RELATIVE_TO_PARENT——父布局尺寸乘以value
        //Value 起始值，根据Type不同取值意义不同
        Animation transAnimation = new TranslateAnimation(Animation.ABSOLUTE, 0f, Animation.ABSOLUTE,
                200f, Animation.ABSOLUTE, 0f, Animation.ABSOLUTE, 200f);

        switch (loadAnimationFromType){
            case XML:
                transAnimation = AnimationUtils.loadAnimation(ViewAnimationActivity.this,
                        R.anim.translate_animation);
                break;
        }

        transAnimation.setDuration(2000);       //动画时间设置
        transAnimation.setFillAfter(false);     //动画结束之后是否停留在结束为止
        transAnimation.setFillBefore(true);     //动画结束之后是否回到开始位置
        transAnimation.setRepeatCount(1);       //动画重复次数，可以指定重复多次播放动画
        transAnimation.setRepeatMode(Animation.REVERSE);    //动画重复播放模式，RESTART——重新开始    REVERSE——动画倒放

        return transAnimation;
    }

    /**
     * 缩放动画
     * @return 动画对象
     */
    private Animation getScaleAnimation(LoadAnimationFromType loadAnimationFromType) {
//        ScaleAnimation(float fromX, float toX, float fromY, float toY,
//        int pivotXType, float pivotXValue, int pivotYType, float pivotYValue)

        //value 起始值，乘以自身尺寸倍数
        //pivotType  缩放圆心取值类型，ABSOLUTE——绝对坐标值
        //                            RELATIVE_TO_SELF——自身尺寸乘以value（如：value= 0.5则表示以自身中心点位原点就行缩放）
        //                            RELATIVE_TO_PARENT——父布局尺寸乘以value
        Animation scaleAnimation = new ScaleAnimation(1f, 1.5f, 1f, 1.5f,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);

        switch (loadAnimationFromType){
            case XML:
                scaleAnimation = AnimationUtils.loadAnimation(ViewAnimationActivity.this,
                        R.anim.scale_animation);
                break;
        }

        scaleAnimation.setDuration(2000);       //动画时间设置
        scaleAnimation.setFillAfter(false);     //动画结束之后是否停留在结束为止
        scaleAnimation.setFillBefore(true);     //动画结束之后是否回到开始位置
        scaleAnimation.setRepeatCount(1);       //动画重复次数，可以指定重复多次播放动画
        scaleAnimation.setRepeatMode(Animation.REVERSE);    //动画重复播放模式，RESTART——重新开始    REVERSE——动画倒放

        return scaleAnimation;
    }

    /**
     * 旋转动画
     * @return 动画对象
     */
    private Animation getRotateAnimation(LoadAnimationFromType loadAnimationFromType) {
//        RotateAnimation(float fromDegrees,     float toDegrees, int pivotXType, float pivotXValue,
//        int pivotYType, float pivotYValue)

        //value 起始值，乘以自身尺寸倍数
        //pivotType  缩放圆心取值类型，ABSOLUTE——绝对坐标值
        //                            RELATIVE_TO_SELF——自身尺寸乘以value（如：value= 0.5则表示以自身中心点位原点就行缩放）
        //                            RELATIVE_TO_PARENT——父布局尺寸乘以value
        Animation rotateAnimation = new RotateAnimation(0, 360,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);

        switch (loadAnimationFromType){
            case XML:
                rotateAnimation = AnimationUtils.loadAnimation(ViewAnimationActivity.this,
                        R.anim.rotate_animation);
                break;
        }

        rotateAnimation.setDuration(2000);       //动画时间设置
        rotateAnimation.setFillAfter(false);     //动画结束之后是否停留在结束为止
        rotateAnimation.setFillBefore(true);     //动画结束之后是否回到开始位置
        rotateAnimation.setRepeatCount(1);       //动画重复次数，可以指定重复多次播放动画
        rotateAnimation.setRepeatMode(Animation.REVERSE);    //动画重复播放模式，RESTART——重新开始    REVERSE——动画倒放

        return rotateAnimation;
    }

    /**
     * 透明度动画
     * @return 动画对象
     */
    private Animation getAlphaAnimation(LoadAnimationFromType loadAnimationFromType) {
        //1——100%不透明
        //0——100%透明
        Animation alphaAnimation = new AlphaAnimation(1f, 0f);

        switch (loadAnimationFromType){
            case XML:
                alphaAnimation = AnimationUtils.loadAnimation(ViewAnimationActivity.this,
                        R.anim.alpha_animation);
                break;
        }

        alphaAnimation.setDuration(2000);       //动画时间设置
        alphaAnimation.setFillAfter(false);     //动画结束之后是否停留在结束为止
        alphaAnimation.setFillBefore(true);     //动画结束之后是否回到开始位置
        alphaAnimation.setRepeatCount(1);       //动画重复次数，可以指定重复多次播放动画
        alphaAnimation.setRepeatMode(Animation.REVERSE);    //动画重复播放模式，RESTART——重新开始    REVERSE——动画倒放

        return alphaAnimation;
    }

    /**
     * AnimationSet
     * @return animationSet
     */
    private Animation getAnimationSet(LoadAnimationFromType loadAnimationFromType){
        AnimationSet animationSet = new AnimationSet(true);
        animationSet.addAnimation(getTransAnimation(loadAnimationFromType));
        animationSet.addAnimation(getScaleAnimation(loadAnimationFromType));
        animationSet.addAnimation(getRotateAnimation(loadAnimationFromType));
        animationSet.addAnimation(getAlphaAnimation(loadAnimationFromType));

        animationSet.setDuration(2000);       //动画时间设置
        animationSet.setFillAfter(false);     //动画结束之后是否停留在结束为止
        animationSet.setFillBefore(true);     //动画结束之后是否回到开始位置
        animationSet.setRepeatCount(1);       //动画重复次数，可以指定重复多次播放动画
        animationSet.setRepeatMode(Animation.REVERSE);    //动画重复播放模式，RESTART——重新开始    REVERSE——动画倒放
        return animationSet;
    }
}
