package com.zhong.media.media.player;

import android.content.Context;
import android.graphics.PixelFormat;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;
import java.util.Map;

public class MySurfaceView extends SurfaceView implements SurfaceHolder.Callback, IPlayer {
    private static final String TAG = MySurfaceView.class.getSimpleName();

    public SurfaceHolder mSurfaceHolder;
    private MediaPlayer mMediaPlayer;

    private Context context;
    // settable by the client
    private Uri mUri;
    private Map<String, String> mHeaders;

    private MediaPlayer.OnPreparedListener onPreparedListener;

    public MySurfaceView(Context context) {
        this(context, null);
    }

    public MySurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;

        this.getHolder().addCallback(this);
        this.getHolder().setFormat(PixelFormat.RGBA_8888);
        this.getHolder().setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        mSurfaceHolder = holder;

        openVideo();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        // after we return from this we can't use the surface any more
        mSurfaceHolder = null;
        release();
    }

    @Override
    public void setVideoPath(String videoPath) {
        setVideoURI(Uri.parse(videoPath));
    }

    @Override
    public void setVideoURI(Uri uri) {
        setVideoURI(uri, null);
    }

    @Override
    public void setVideoURI(Uri uri, Map<String, String> headers) {
        mUri = uri;
        mHeaders = headers;

        openVideo();
    }

    private void openVideo() {
        if (mUri == null || mSurfaceHolder == null) {
            // not ready for playback just yet, will try again later
            Log.d(TAG, "mUri == null or surfaceHolder == null");
            return;
        }
        // we shouldn't clear the target state, because somebody might have
        // called start() previously
        release();

        try {
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setOnPreparedListener(mPreparedListener);
//            mMediaPlayer.setOnVideoSizeChangedListener(mSizeChangedListener);
//            mMediaPlayer.setOnCompletionListener(mCompletionListener);
//            mMediaPlayer.setOnErrorListener(mErrorListener);
//            mMediaPlayer.setOnInfoListener(mInfoListener);
//            mMediaPlayer.setOnBufferingUpdateListener(mBufferingUpdateListener);
            mMediaPlayer.setDataSource(context, mUri, mHeaders);
            mMediaPlayer.setDisplay(mSurfaceHolder);
            mMediaPlayer.setScreenOnWhilePlaying(true);

            //使用prepareAsync必须在onPrepared之后调用mMediaPlayer.start()
            mMediaPlayer.prepareAsync();
//            mMediaPlayer.start();

        } catch (IOException ex) {
            Log.w(TAG, "Unable to open content: " + mUri, ex);
        } catch (IllegalArgumentException ex) {
            Log.w(TAG, "Unable to open content: " + mUri, ex);
        }
    }


    MediaPlayer.OnPreparedListener mPreparedListener = new MediaPlayer.OnPreparedListener() {
        @Override
        public void onPrepared(MediaPlayer mp) {
            start();

            if(onPreparedListener != null){
                onPreparedListener.onPrepared(mp);
            }
        }
    };

    @Override
    public void start() {
        if (mMediaPlayer != null) {
            mMediaPlayer.start();
        }
    }

    @Override
    public void pause() {
        if(mMediaPlayer != null){
            if(mMediaPlayer.isPlaying()){
                mMediaPlayer.pause();
            }
        }
    }

    @Override
    public void stop() {
        if (mMediaPlayer != null) {
            mMediaPlayer.stop();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    @Override
    public void release() {
        try {
            if (mMediaPlayer != null) {
                mMediaPlayer.reset();
                mMediaPlayer.release();
                mMediaPlayer = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isPlaying() {
        if(mMediaPlayer != null){
           return mMediaPlayer.isPlaying();
        } else{
            return false;
        }
    }

    @Override
    public void setOnPreparedListener(MediaPlayer.OnPreparedListener onPreparedListener) {
        this.onPreparedListener = onPreparedListener;
    }

    @Override
    public MediaPlayer getMediaPlayer() {
        return mMediaPlayer;
    }

}
