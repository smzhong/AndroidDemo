package com.zhong.property_animation;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.Toast;

import com.zhong.main.BaseActivity;
import com.zhong.main.R;

import butterknife.BindView;
import butterknife.OnClick;

public class ViewPropertyAnimatorActivity extends BaseActivity {

    @BindView(R.id.imageView)
    ImageView imageView;

    public static void startSelf(Context context) {
        Intent intent = new Intent(context, ViewPropertyAnimatorActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected int initLayout() {
        return R.layout.activity_view_property_animator;
    }

    @Override
    protected void create() {

    }

    @OnClick(R.id.start_btn)
    protected void onClick(View view){
        switch (view.getId()){
            case R.id.start_btn:
                imageView.animate().rotation(180).alpha(0.2f).translationXBy(200)
                        .setDuration(5000).setInterpolator(new LinearInterpolator())
                        //开启硬件加速
                        .withLayer()
                        .withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                //动画结束触发任务
                                Toast.makeText(ViewPropertyAnimatorActivity.this,"StartRunnable",Toast.LENGTH_SHORT).show();
                            }
                        }).withStartAction(new Runnable() {
                            @Override
                            public void run() {
                                //动画开始触发任务
                                Toast.makeText(ViewPropertyAnimatorActivity.this,"StartRunnable",Toast.LENGTH_SHORT).show();
                            }
                        });

                Person.getInstance().setAge(18).setSex("female").setName("Jane");
                break;
        }
    }
}
