package com.zhong.media;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.danikula.videocache.CacheListener;
import com.zhong.media.media.media2.MySurfaceView2;

import java.io.File;

public class MediaPlayerActivity extends Activity implements CacheListener {
	private static final String TAG = MediaPlayerActivity.class.getSimpleName();

	private String url;
	private MySurfaceView2 surfaceView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_media_player);
		
		Intent intent = getIntent();
		if(intent != null){
			url = intent.getStringExtra("url");
		}
		
		surfaceView = findViewById(R.id.surface_view);

//		surfaceView.setOnPlayerCallback(new OnPlayerCallback() {
//			@Override
//			public void onVideoStart(Uri mUri, int index) {
//
//			}
//
//			@Override
//			public void onVideoComplete(Uri mUri, int index) {
//
//			}
//
//			@Override
//			public void onVideoError(Uri mUri, int index) {
//
//			}
//
//			@Override
//			public void onVideoSizeChangedListener(boolean isFullScreen) {
//
//			}
//
//			@Override
//			public void onLoadingChanged(boolean isShow) {
//
//			}
//		});

//		surfaceView.setVideoPath(url);
//		surfaceView.startVideo();
		startVideo();
	}


	private void startVideo() {
//		HttpProxyCacheServer proxy = MyApplication.getProxy(this);
//		proxy.registerCacheListener(this, url);
//		String proxyUrl = proxy.getProxyUrl(url);
//		Log.d(TAG, "Use proxy url " + proxyUrl + " instead of original url " + url);
		surfaceView.setVideoPath(url);
		surfaceView.startVideo();
	}

	@Override
	public void onCacheAvailable(File cacheFile, String url, int percentsAvailable) {
		Log.d(TAG, String.format("onCacheAvailable. percents: %d, file: %s, url: %s", percentsAvailable, cacheFile, url));
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		MyApplication.getProxy(this).unregisterCacheListener(this);
	}
}
