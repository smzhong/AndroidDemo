package com.zhong.mvp2.presenter;

import com.zhong.mvp2.view.IView;

/**
 * <pre>
 *     author: Zhong
 *     time  : 2018/12/14
 *     desc  :
 * </pre>
 */
public interface IPresenter {
    void attachView(IView view);
    void detachView();
}
