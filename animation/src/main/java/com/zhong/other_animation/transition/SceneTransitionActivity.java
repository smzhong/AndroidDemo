package com.zhong.other_animation.transition;

import android.content.Context;
import android.content.Intent;
import android.transition.ChangeBounds;
import android.transition.Scene;
import android.transition.TransitionManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.zhong.main.BaseActivity;
import com.zhong.main.R;

import butterknife.BindView;
import butterknife.OnClick;

public class SceneTransitionActivity extends BaseActivity {

    @BindView(R.id.transition_root_view)
    ViewGroup transitionRootView;

    ImageView imageView1;

    private Scene scene1;
    private Scene scene2;
    private boolean isScene2 = false;

    public static void startSelf(Context context){
        Intent intent = new Intent(context, SceneTransitionActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected int initLayout() {
        return R.layout.activity_sence_transition;
    }

    @Override
    protected void create() {
        initScene();
    }

    private void initScene() {
        scene1 = Scene.getSceneForLayout(transitionRootView, R.layout.transition_scene_11, this);
        scene2 = Scene.getSceneForLayout(transitionRootView, R.layout.transition_scene_22, this);
//        TransitionManager.go(scene1);
    }

    @OnClick(R.id.transition_btn)
    protected void onClick(){
        TransitionManager.go(isScene2?scene1:scene2,new ChangeBounds());
        isScene2=!isScene2;
    }

    @OnClick({R.id.imageView1,R.id.imageView2,R.id.imageView3,R.id.imageView4})
    protected void onSceneElementClick(View view){
        String tag = "default";
        switch (view.getId()){
            case R.id.imageView1:
                tag = "imageView1";
                break;
            case R.id.imageView2:
                tag = "imageView2";
                break;
            case R.id.imageView3:
                tag = "imageView3";
                break;
            case R.id.imageView4:
                tag = "imageView4";
                break;
        }

        Toast.makeText(this,tag, Toast.LENGTH_SHORT).show();
    }
}
