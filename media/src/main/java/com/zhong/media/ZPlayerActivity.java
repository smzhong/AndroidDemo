package com.zhong.media;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.danikula.videocache.CacheListener;
import com.zhong.media.media.listener.OnPlayerCallback;
import com.zhong.media.media.player.ZVideoPlayer;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ZPlayerActivity extends AppCompatActivity implements CacheListener {
    private static final String TAG = ZPlayerActivity.class.getSimpleName();

    private String url;
    private ZVideoPlayer player;

    private Button startBtn;
    private Button pauseBtn;
    private Button stopBtn;
    private Button fullBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zplayer);

        Intent intent = getIntent();
        if(intent != null){
            url = intent.getStringExtra("url");
        }

        initPlayer();
        initView();
    }

    private void initView() {
        startBtn = findViewById(R.id.start_btn);
        pauseBtn = findViewById(R.id.pause_btn);
        stopBtn = findViewById(R.id.stop_btn);
        fullBtn = findViewById(R.id.full_btn);

        startBtn.setOnClickListener(onClickListener);
        pauseBtn.setOnClickListener(onClickListener);
        stopBtn.setOnClickListener(onClickListener);
        fullBtn.setOnClickListener(onClickListener);
    }

    private void initPlayer() {
        player = findViewById(R.id.surface_view);

        player.setOnPlayerCallback(new OnPlayerCallback() {
            @Override
            public void onVideoStart(Uri mUri, int index) {

            }

            @Override
            public void onVideoComplete(Uri mUri, int index) {

            }

            @Override
            public void onVideoError(Uri mUri, int index) {

            }

            @Override
            public void onVideoStop(int retryCount, Uri mUri, int index) {

            }

            @Override
            public void onVideoSizeChangedListener(boolean isFullScreen) {

            }

            @Override
            public void onLoadingChanged(boolean isShow) {

            }
        });
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.start_btn:
                    player.setPlayerType(ZVideoPlayer.TEXTURE_VIEW_TYPE);
//                    HttpProxyCacheServer proxy = MyApplication.getProxy(ZPlayerActivity.this);
//                    proxy.registerCacheListener(ZPlayerActivity.this, url);
//                    String proxyUrl = proxy.getProxyUrl(url);
//                    Log.d(TAG, "Use proxy url " + proxyUrl + " instead of original url " + url);
//                    player.setVideoPath(url);

                    List<String> list = new ArrayList<>();
                    list.add("http://htrip-film.oss-cn-hangzhou.aliyuncs.com/movie/mov/58625_2020030913402319286.mp4");
                    list.add("http://htrip-film.oss-cn-hangzhou.aliyuncs.com/movie/mov/69276_2020030611142392592.mp4");
                    player.setVideoPathList(list);
                    break;
                case R.id.pause_btn:
                    player.pauseVideo();
                    break;
                case R.id.stop_btn:
                    player.stopVideo();
                    break;
                case R.id.full_btn:
                    break;
            }
        }
    };

    @Override
    public void onCacheAvailable(File cacheFile, String url, int percentsAvailable) {
        Log.d(TAG, String.format("onCacheAvailable. percents: %d, file: %s, url: %s", percentsAvailable, cacheFile, url));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(player != null){
            player.releasePlayer();
            player = null;
        }

        MyApplication.getProxy(this).unregisterCacheListener(this);
    }
}
