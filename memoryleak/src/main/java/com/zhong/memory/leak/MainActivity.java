package com.zhong.memory.leak;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.zhong.memory.leak.object.MyObject;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    private MyObject myObject = new MyObject("测试对象");
    private Button button;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                test(myObject);
                myObject = null;
            }
        });
    }

    private void test(final MyObject myObject) {
        Log.d(TAG, "开始测试");

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                Log.d(TAG, "异步任务结果打印：" + myObject.getName());
            }
        }).start();

        new AsyncTask<String, Integer, String>() {
            @Override
            protected String doInBackground(String... arg0) {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                Log.d(TAG, "异步任务结果打印：" + myObject.getName());
            }
        }.execute("");
    }

//    private void test(){
//        asyncTask.execute(myObject);
//        myObject = null;
//    }
//
//    AsyncTask asyncTask = new AsyncTask<MyObject,Integer, MyObject>() {
//        @Override
//        protected MyObject doInBackground(MyObject... myObjects) {
//            try {
//                Thread.sleep(3000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            return myObjects[0];
//        }
//
//        @Override
//        protected void onPostExecute(MyObject s) {
//            super.onPostExecute(s);
//            Log.d(TAG,"异步任务结果打印：" + s.getName());
//        }
//    };
}
