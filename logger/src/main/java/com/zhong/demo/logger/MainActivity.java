package com.zhong.demo.logger;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;


import com.orhanobut.logger.Logger;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
    }

    @OnClick(R.id.log_d_btn)
    protected void onClick(){
//        Logger.d("Log d");
//        Logger.e("Log e");
//        Logger.w("Log w");
//        Logger.v("Log v");
//        Logger.i("Log i");
//        Logger.wtf("Log wtf");
//
//
//        Logger.t("MyTag").d("用临时TAG打印");


//        try {
//            int[] a = {1,2,3};
//            a[4] = 4;
//        } catch (Exception e) {
//            e.printStackTrace();
//            Logger.e(e, "message");
//        }

        String jsonStr = "{\"code\":0,\"msg\":\"获取成功\",\"data\":[{\"categoryName\":\"私人专属\",\"categoryId\":\"18\"},{\"categoryName\":\"必看电影\",\"categoryId\":\"19\"},{\"categoryName\":\"猜你喜欢\",\"categoryId\":\"17\"}]}";
        String xmlStr = "<?xml version=\"1.0\" encoding=\"utf-8\"?><android.support.constraint.ConstraintLayout xmlns:android=\"http://schemas.android.com/apk/res/android\" xmlns:app=\"http://schemas.android.com/apk/res-auto\" xmlns:tools=\"http://schemas.android.com/tools\" android:layout_width=\"match_parent\" android:layout_height=\"match_parent\" tools:context=\".MainActivity\">  <Button android:id=\"@+id/log_d_btn\" android:layout_width=\"wrap_content\" android:layout_height=\"wrap_content\" android:layout_marginStart=\"32dp\" android:layout_marginTop=\"16dp\" android:text=\"Log.d\" android:textAllCaps=\"false\" app:layout_constraintStart_toStartOf=\"parent\" app:layout_constraintTop_toTopOf=\"parent\"/> </android.support.constraint.ConstraintLayout>";

        Logger.json(jsonStr);
        Logger.xml(xmlStr);
    }
}
