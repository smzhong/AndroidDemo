package com.zhong.demo.viewstub;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private Button visibleBtn;
    private ViewStub visibleViewStub;

    private Button inflateBtn;
    private ViewStub inflateViewStub;

    private View viewStubContentView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        visibleBtn = findViewById(R.id.view_stub_visible_btn);
        visibleViewStub = findViewById(R.id.visible_view_stub);

        visibleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                visibleViewStub.setVisibility(View.VISIBLE);
            }
        });

        inflateBtn = findViewById(R.id.view_stub_inflate_btn);
        inflateViewStub = findViewById(R.id.inflate_view_stub);

        inflateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(viewStubContentView == null){
                    viewStubContentView = inflateViewStub.inflate();
                }
            }
        });
    }
}
