package com.zhong.property_animation;

import android.animation.ObjectAnimator;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.zhong.main.R;
import com.zhong.view_animation.interpolator.ParabolaInterpolator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ValueAnimatorActivity extends AppCompatActivity {
    private static final String TAG = ValueAnimatorActivity.class.getSimpleName();

    @BindView(R.id.text_view)
    TextView textView;
    @BindView(R.id.star_iv)
    ImageView starImageView;

    public static void startSelf(Context context) {
        Intent intent = new Intent(context, ValueAnimatorActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_value_animator);

        ButterKnife.bind(this);
    }

    @OnClick({R.id.text_color_btn, R.id.count_down_btn, R.id.move_start_btn, R.id.view_property_btn})
    protected void onClick(View view) {
        switch (view.getId()) {
            case R.id.text_color_btn:
//                ObjectAnimator textColorAnimator = ObjectAnimator.ofFloat(textView,"x",0, 200);

                ObjectAnimator textColorAnimator = ObjectAnimator.ofArgb(textView, "textColor", 0xffff0000, 0xff00ff00);
                textColorAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        //同样可以通过监听动画变化过程，在此处为View设置属性
                    }
                });
                textColorAnimator.setDuration(3000);
                textColorAnimator.setRepeatCount(1);
                textColorAnimator.setRepeatMode(ValueAnimator.REVERSE);
                textColorAnimator.start();
                break;
            case R.id.count_down_btn:
                ValueAnimator countDownAnimator = ValueAnimator.ofObject(new TypeEvaluator<float[]>() {
                    @Override
                    public float[] evaluate(float fraction, float[] startValue, float[] endValue) {
                        float[] temp = new float[2];
                        temp[0] = fraction * (endValue[0] - startValue[0]) + startValue[0];
                        temp[1] = fraction * (endValue[1] - startValue[1]) + startValue[1];
                        return temp;
                    }
                },new float[]{100f, 200f}, new float[]{300f, 400f});
                countDownAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        float[] xyPos = (float[]) animation.getAnimatedValue();
                        textView.setHeight((int) xyPos[0]);   //通过属性值设置View属性动画
                        textView.setWidth((int) xyPos[1]);    //通过属性值设置View属性动画
                    }
                });

                countDownAnimator.setInterpolator(new ParabolaInterpolator());
                countDownAnimator.setDuration(1000);
                countDownAnimator.setRepeatCount(5);
                countDownAnimator.setRepeatMode(ValueAnimator.REVERSE);
                countDownAnimator.start();

                break;
            case R.id.move_start_btn:

//                ValueAnimator valueAnimator = new ValueAnimator();
////                valueAnimator.setObjectValues(new Point(0, 0), new Point(300, 400));
////
////                moveAnimator.setEvaluator(new TypeEvaluator<Point>() {
////                    @Override
////                    public Point evaluate(float fraction, Point startValue, Point endValue) {
////                        Point point = new Point();
////
////                        point.x = (int) (startValue.x + fraction * (endValue.x - startValue.x));
////                        point.y = (int) (startValue.y + fraction * (endValue.y - startValue.y));
////                        return point;
////                    }
////                });

                ValueAnimator valueAnimator = ValueAnimator.ofObject(new TypeEvaluator<Point>() {
                    @Override
                    public Point evaluate(float fraction, Point startValue, Point endValue) {
                        Point point = new Point();

                        point.x = (int) (startValue.x + fraction * (endValue.x - startValue.x));
                        point.y = (int) (startValue.y + fraction * (endValue.y - startValue.y));
                        return point;
                    }
                }, new Point(0, 0), new Point(300, 400));

                valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        Point point = (Point) animation.getAnimatedValue();

                        starImageView.setTranslationX(point.x);
                        starImageView.setTranslationY(point.y);
                    }
                });
                valueAnimator.setInterpolator(new OvershootInterpolator());
                valueAnimator.setDuration(3000);
                valueAnimator.setRepeatCount(1);
                valueAnimator.setRepeatMode(ValueAnimator.REVERSE);
                valueAnimator.start();
                break;
            case R.id.view_property_btn:
                starImageView.animate().translationX(200).translationY(200).setDuration(2000).setUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        float value = (float) animation.getAnimatedValue();

                        Log.d(TAG, "View Property Animator values = " + value);
                    }
                }).start();
                break;
        }
    }
}
