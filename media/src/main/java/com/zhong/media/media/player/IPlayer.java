package com.zhong.media.media.player;

import android.media.MediaPlayer;
import android.net.Uri;

import java.util.Map;

/**
 * <pre>
 *     author: Zhong
 *     time  : 2020/3/11
 *     desc  :
 * </pre>
 */
public interface IPlayer {
    void setVideoPath(String videoPath);
    void setVideoURI(Uri uri);
    void setVideoURI(Uri uri, Map<String, String> headers);

    void start();
    void pause();
    void stop();
    void release();

    boolean isPlaying();

    void setOnPreparedListener(MediaPlayer.OnPreparedListener onPreparedListener);
//    void setOnCompletionListener(MediaPlayer.OnCompletionListener onCompletionListener);
//    void setOnErrorListener(MediaPlayer.OnErrorListener onErrorListener);
//    void setOnInfoListener(MediaPlayer.OnInfoListener onInfoListener);
//    void setOnBufferingUpdateListener(MediaPlayer.OnBufferingUpdateListener onBufferingUpdateListener);

    MediaPlayer getMediaPlayer();
}
