package com.zhong.drawable_animtion;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import com.zhong.main.BaseActivity;
import com.zhong.main.R;

import butterknife.BindView;
import butterknife.OnClick;

public class DrawableAnimationActivity extends BaseActivity {
    @BindView(R.id.drawable_anim_iv)
    ImageView drawableAnimIV;

    public static void startSelf(Context context){
        Intent intent = new Intent(context, DrawableAnimationActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected int initLayout() {
        return R.layout.activity_drawable_animation;
    }

    @Override
    protected void create() {

    }

    @OnClick(R.id.drawable_anim_iv)
    protected void onClick(){
        AnimationDrawable animationDrawable = (AnimationDrawable)(drawableAnimIV.getDrawable());

        if(animationDrawable.isRunning()){
            animationDrawable.stop();
        } else{
            animationDrawable.start();
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
    }
}
