package com.zhong.property_animation;

import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.CycleInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.TextView;
import android.widget.Toast;

import com.zhong.main.BaseActivity;
import com.zhong.main.R;
import com.zhong.property_animation.widget.InterpolatorView;
import com.zhong.view_animation.interpolator.ParabolaInterpolator;

import butterknife.BindView;
import butterknife.OnClick;

public class TestInterpolatorViewActivity extends BaseActivity {
    @BindView(R.id.interpolator_view)
    InterpolatorView interpolatorView;
    @BindView(R.id.interpolator_tv)
    TextView mInterpolatorTv;

    private Interpolator mInterpolator = new ParabolaInterpolator();

    public static void startSelf(Context context){
        Intent intent = new Intent(context, TestInterpolatorViewActivity.class);
        context.startActivity(intent);
    }


    @Override
    protected int initLayout() {
        return R.layout.activity_test_interpolator_view;
    }

    @Override
    protected void create() {

        mInterpolatorTv.setText("ParabolaInterpolator");
        loadToolbar();
    }

    @OnClick({R.id.show_btn,R.id.start_anim_btn})
    protected void onClick(View view){
        switch (view.getId()){
            case R.id.show_btn:
                if(mInterpolator == null){
                    Toast.makeText(TestInterpolatorViewActivity.this,"Please select interpolator first", Toast.LENGTH_SHORT).show();
                    return;
                }
                interpolatorView.setInterpolator(mInterpolator);
                break;
            case R.id.start_anim_btn:
                interpolatorView.startAnimation(1500);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.view_animation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        String interpolatorName = "";
        switch (id){
            case R.id.clear:
                mInterpolator = null;
                break;
            case R.id.AccelerateDecelerateInterpolator:
                //在动画开始与结束的地方速率改变比较慢，在中间的时候加速
                interpolatorName = "AccelerateDecelerateInterpolator";
                mInterpolator = new AccelerateDecelerateInterpolator();
                break;
            case R.id.AccelerateInterpolator:
                //在动画开始的地方速率改变比较慢，然后开始加速
                interpolatorName = "AccelerateInterpolator";
                mInterpolator = new AccelerateInterpolator();
                break;
            case R.id.AnticipateInterpolator:
                //开始的时候向后然后向前甩
                interpolatorName = "AnticipateInterpolator";
                mInterpolator = new AnticipateInterpolator();
                break;
            case R.id.AnticipateOvershootInterpolator:
                //开始的时候向后然后向前甩一定值后返回最后的值
                interpolatorName = "AnticipateOvershootInterpolator";
                mInterpolator = new AnticipateOvershootInterpolator();
                break;
            case R.id.BounceInterpolator:
                //动画结束的时候弹起
                interpolatorName = "BounceInterpolator";
                mInterpolator = new BounceInterpolator();
                break;
            case R.id.CycleInterpolator:
                //动画循环播放特定的次数，速率改变沿着正弦曲线
                interpolatorName = "CycleInterpolator";
                mInterpolator = new CycleInterpolator(0.5f);
                break;
            case R.id.DecelerateInterpolator:
                //在动画开始的地方快然后慢
                interpolatorName = "DecelerateInterpolator";
                mInterpolator = new DecelerateInterpolator();
                break;
            case R.id.LinearInterpolator:
                //以常量速率改变
                interpolatorName = "LinearInterpolator";
                mInterpolator = new LinearInterpolator();
                break;
            case R.id.OvershootInterpolator:
                //向前甩一定值后再回到原来位置
                interpolatorName = "OvershootInterpolator";
                mInterpolator = new OvershootInterpolator();
                break;
        }

        mInterpolatorTv.setText(interpolatorName);
        return super.onOptionsItemSelected(item);
    }
}
