package com.zhong.property_animation;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.zhong.main.BaseActivity;
import com.zhong.main.R;

import butterknife.OnClick;

public class PropertyAnimationActivity extends BaseActivity {

    public static void startSelf(Context context){
        Intent intent = new Intent(context, PropertyAnimationActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected int initLayout() {
        return R.layout.activity_property_animation;
    }

    @Override
    protected void create() {
    }

    @OnClick({R.id.object_animator_btn,R.id.value_animator_btn, R.id.layout_transition_btn,
            R.id.xiaomi_btn, R.id.test_interpolator_view_btn, R.id.view_property_animator_btn})
    protected void onClick(View view){
        switch (view.getId()){
            case R.id.object_animator_btn:
                ObjectAnimatorActivity.startSelf(PropertyAnimationActivity.this);
                break;
            case R.id.value_animator_btn:
                ValueAnimatorActivity.startSelf(PropertyAnimationActivity.this);
                break;
            case R.id.layout_transition_btn:
                LayoutTransitionActivity.startSelf(PropertyAnimationActivity.this);
                break;
            case R.id.xiaomi_btn:
                XiaomiActivity.startSelf(PropertyAnimationActivity.this);
                break;
            case R.id.test_interpolator_view_btn:
                TestInterpolatorViewActivity.startSelf(PropertyAnimationActivity.this);
                break;
            case R.id.view_property_animator_btn:
                ViewPropertyAnimatorActivity.startSelf(PropertyAnimationActivity.this);
                break;
        }
    }
}
