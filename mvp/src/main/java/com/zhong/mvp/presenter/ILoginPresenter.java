package com.zhong.mvp.presenter;

import com.zhong.mvp.bean.UserBean;

/**
 * <pre>
 *     author: Zhong
 *     time  : 2018/12/8
 *     desc  :
 * </pre>
 */
public interface ILoginPresenter {

    /**
     * 获取登录数据
     * @param param 参数
     */
    void getLoginData(UserBean param);
}
