package com.zhong.other_animation;

import android.content.Context;
import android.content.Intent;

import com.zhong.main.BaseActivity;
import com.zhong.main.R;

/**
 * 触摸动画
 */
public class TouchFeedbackActivity extends BaseActivity {

    public static void startSelf(Context context){
        Intent intent = new Intent(context, TouchFeedbackActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected int initLayout() {
        return R.layout.activity_touch_feedback;
    }

    @Override
    protected void create() {

    }
}
