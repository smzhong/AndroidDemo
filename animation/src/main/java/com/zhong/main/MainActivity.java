package com.zhong.main;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.zhong.drawable_animtion.DrawableAnimationActivity;
import com.zhong.main.adapter.AnimationAdapter;
import com.zhong.other_animation.OtherAnimationActivity;
import com.zhong.property_animation.PropertyAnimationActivity;
import com.zhong.view_animation.AnimationActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class MainActivity extends BaseActivity {

    private List<String> datas;
    private AnimationAdapter adapter;

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    @Override
    protected int initLayout() {
        return R.layout.activity_main;
    }

    @Override
    protected void create() {
        initData();
        initRecyclerView();
    }

    private void initData(){
        datas = new ArrayList<>();
        datas.add("View Animation");
        datas.add("Drawable Animation");
        datas.add("Property Animation");
        datas.add("5.0+ Animation");
    }

    private void initRecyclerView() {
        adapter = new AnimationAdapter(MainActivity.this, datas);
        adapter.setOnClickListener(onItemClickListener);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(MainActivity.this, 2);
        gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(gridLayoutManager);
        mRecyclerView.setAdapter(adapter);
    }

    AnimationAdapter.OnItemClickListener onItemClickListener = new AnimationAdapter.OnItemClickListener() {
        @Override
        public void onClick(int position, View view) {
            switch (position){
                case 0:
                    AnimationActivity.startSelf(MainActivity.this);
                    break;
                case 1:
                    DrawableAnimationActivity.startSelf(MainActivity.this);
                    break;
                case 2:
                    PropertyAnimationActivity.startSelf(MainActivity.this);
                    break;
                case 3:
                    OtherAnimationActivity.startSelf(MainActivity.this);
                    break;
            }
        }
    };
}
