package com.zhong.main.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zhong.main.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * <pre>
 *     author: Zhong
 *     time  : 2018/12/27
 *     desc  :
 * </pre>
 */
public class AnimationAdapter extends RecyclerView.Adapter<AnimationAdapter.MyViewHolder> {
    private static final String TAG = AnimationAdapter.class.getSimpleName();

    private Context mContext;
    private List<String> datas;
    private OnItemClickListener onClickListener;

    public AnimationAdapter(Context mContext, List<String> datas){
        this.mContext = mContext;
        this.datas = datas;
    }

    public AnimationAdapter(Context mContext, List<String> datas, OnItemClickListener onClickListener){
        this.mContext = mContext;
        this.datas = datas;
        this.onClickListener = onClickListener;
    }

    public void setOnClickListener(OnItemClickListener onClickListener){
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        MyViewHolder myViewHolder = new MyViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_main_layout, viewGroup, false));
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, final int i) {
        String  name = datas.get(i);
        if(onClickListener != null){
            viewHolder.itemRel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickListener.onClick(i, v);
                }
            });
        }

        viewHolder.nameTv.setText(name);
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.item_rl)
        RelativeLayout itemRel;
        @BindView(R.id.item_iv)
        ImageView imageView;
        @BindView(R.id.item_name_tv)
        TextView nameTv;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this,itemView);
        }
    }

    public interface OnItemClickListener{
        void onClick(int position, View view);
    }
}
