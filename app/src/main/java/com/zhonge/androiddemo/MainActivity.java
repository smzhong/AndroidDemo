package com.zhonge.androiddemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.zhonge.androiddemo.broadcast.HomeKeyListenerBroadcast;

public class MainActivity extends AppCompatActivity {
    private HomeKeyListenerBroadcast homeKeyListenerBroadcast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                startActivity(intent);
            }
        });


        homeKeyListenerBroadcast = new HomeKeyListenerBroadcast(this, new HomeKeyListenerBroadcast.OnHomeKeyPressListener() {
            @Override
            public void onHomeKeyPress() {
                Toast.makeText(MainActivity.this, "按下Home键", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onRecentAppsKeyLongPress() {
                Toast.makeText(MainActivity.this, "按下多任务键", Toast.LENGTH_SHORT).show();
            }
        });

        homeKeyListenerBroadcast.registerSelf();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(homeKeyListenerBroadcast != null){
            homeKeyListenerBroadcast.unRegisterSelf();
        }
    }
}
