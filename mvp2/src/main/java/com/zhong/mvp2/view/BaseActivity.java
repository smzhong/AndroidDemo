package com.zhong.mvp2.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.zhong.mvp2.presenter.IPresenter;

/**
 * <pre>
 *     author: Zhong
 *     time  : 2018/12/14
 *     desc  :
 * </pre>
 */
public abstract class BaseActivity<P extends IPresenter> extends AppCompatActivity implements IView{
    //定义Presenter的泛型进行约束
    protected P mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(initLayout() instanceof Integer){
            setContentView((Integer) initLayout());
        } else if(initLayout() instanceof View){
            setContentView((View) initLayout());
        } else{
            throw new IllegalArgumentException("initLayout() 应该返回Int或者View类型对象");
        }

        //初始化Presenter
        mPresenter = initPresenter();
        //Presenter与View进行绑定
        mPresenter.attachView(this);

        create();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //Presenter与View解除绑定
        mPresenter.detachView();
    }

    /** 初始化Presenter的抽象方法 */
    protected abstract P initPresenter();
    /** 初始化布局的抽象方法 */
    protected abstract Object initLayout();
    /** Activity OnCreate之后的create抽象方法 */
    protected abstract void create();
}
