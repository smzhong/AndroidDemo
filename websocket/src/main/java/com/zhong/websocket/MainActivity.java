package com.zhong.websocket;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements ServerConnection.ServerListener{
    private static final String TAG = MainActivity.class.getSimpleName();

    @BindView(R.id.msg_et)
    EditText msgEt;
    @BindView(R.id.connect_btn)
    Button connectBtn;
    @BindView(R.id.send_btn)
    Button sendBtn;
    @BindView(R.id.close_btn)
    Button closeBtn;

//    private String wsUrl = "ws://121.40.165.18:8800";
    private String wsUrl = "ws://192.168.1.210:9501?contract=11111&room=1101";
    private ServerConnection mServerConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        mServerConnection = new ServerConnection(wsUrl);
    }

    @OnClick({R.id.connect_btn,R.id.send_btn, R.id.close_btn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.connect_btn:
                mServerConnection.connect(this);
                break;
            case R.id.send_btn:
                String msg = msgEt.getText().toString().trim();
                if(TextUtils.isEmpty(msg)){
                    Toast.makeText(MainActivity.this, "输入message", Toast.LENGTH_SHORT).show();
                } else{
                    mServerConnection.sendMessage(msg);
                }
                break;
            case R.id.close_btn:
                mServerConnection.disconnect();
                break;
        }
    }

    @Override
    public void onNewMessage(String message) {
        Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStatusChange(ServerConnection.ConnectionStatus status) {
//        Toast.makeText(MainActivity.this, , Toast.LENGTH_SHORT).show();
    }
}
