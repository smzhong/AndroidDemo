package com.zhong.other_animation.transition;

import android.content.Context;
import android.content.Intent;
import android.transition.TransitionInflater;
import android.transition.TransitionManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.zhong.main.BaseActivity;
import com.zhong.main.R;

import butterknife.BindView;
import butterknife.OnClick;

public class BeginTransitionActivity extends BaseActivity {

    @BindView(R.id.scene_root_rel)
    RelativeLayout sceneRootRel;
    @BindView(R.id.imageView1)
    ImageView imageView1;
    @BindView(R.id.imageView2)
    ImageView imageView2;
    @BindView(R.id.imageView3)
    ImageView imageView3;
    @BindView(R.id.imageView4)
    ImageView imageView4;

    private boolean isImageBigger = false;
    private int primaryWidth;
    private int primaryHeight;

    public static void startSelf(Context context){
        Intent intent = new Intent(context, BeginTransitionActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected int initLayout() {
        return R.layout.activity_begin_transition;
    }

    @Override
    protected void create() {

    }

    @OnClick({R.id.imageView1, R.id.imageView2, R.id.imageView3, R.id.imageView4})
    protected void onClick(View view) {
        //start scene 是当前的scene
        TransitionManager.beginDelayedTransition(sceneRootRel,
                TransitionInflater.from(this).inflateTransition(R.transition.explode_and_transform));
        //next scene 此时通过代码已改变了scene statue
        changeScene(view);
    }

    private void changeScene(View view) {
        changeSize(view);
        changeVisibility(imageView1, imageView2, imageView3, imageView4);
        view.setVisibility(View.VISIBLE);
    }

    /**
     * view的宽高1.5倍和原尺寸大小切换 * 配合ChangeBounds实现缩放效果 * @param view
     */
    private void changeSize(View view) {
        isImageBigger = !isImageBigger;

        int width = sceneRootRel.getWidth();
        int height = sceneRootRel.getHeight();

        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (isImageBigger) {
            primaryWidth = view.getWidth();
            primaryHeight = view.getHeight();
            layoutParams.width = width;
            layoutParams.height = height;
        } else {
            layoutParams.width = primaryWidth;
            layoutParams.height = primaryHeight;
        }
        view.setLayoutParams(layoutParams);
    }

    /**
     * VISIBLE和INVISIBLE状态切换 * @param views
     */
    private void changeVisibility(View... views) {
        for (View view : views) {
            view.setVisibility(view.getVisibility() == View.VISIBLE ? View.INVISIBLE : View.VISIBLE);
        }
    }

}
