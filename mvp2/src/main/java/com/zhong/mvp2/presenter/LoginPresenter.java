package com.zhong.mvp2.presenter;


import com.zhong.mvp2.bean.UserBean;
import com.zhong.mvp2.contract.LoginContract;
import com.zhong.mvp2.model.LoginModel;
import com.zhong.mvp2.view.LoginActivity;

/**
 * <pre>
 *     author: Zhong
 *     time  : 2018/12/8
 *     desc  :
 * </pre>
 */
public class LoginPresenter extends BasePresenter<LoginActivity, LoginModel> implements LoginContract.ILoginPresenter {

    @Override
    public void getLoginData(UserBean userBean) {
        mView.showLoading();

        mModel.doLogin(userBean, new LoginContract.ILoginModel.LoginCallBack() {
            @Override
            public void onSuccess(UserBean data) {
                mView.showLoginSuccess(data);
            }

            @Override
            public void onFailure(String data) {
                mView.showFailureMessage(data);
            }

            @Override
            public void onError(String error) {
                mView.showErrorMessage(error);
            }

            @Override
            public void onComplete() {
                mView.hideLoading();
            }
        });
    }

    @Override
    protected LoginModel initModel() {
        return new LoginModel();
    }
}
