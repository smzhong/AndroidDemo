package com.zhong.property_animation;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.LayoutTransition;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.zhong.main.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 实现小米计算器科学计算器与普通计算器的切换动画效果
 */
public class XiaomiActivity extends AppCompatActivity {
    @BindView(R.id.calculator_rel)
    RelativeLayout calculatorRel;       //总布局，需要指定LayoutTransition
    @BindView(R.id.calculator_top_lin)
    LinearLayout calculatorTopLin;      //三角函数计算区
    @BindView(R.id.calculator_left_lin)
    LinearLayout calculatorLeftLin;     //其他计算区
    @BindView(R.id.calculator_num_lin)
    LinearLayout calculatorNumLin;      //数字键盘区
    @BindView(R.id.change_btn)
    Button translateBtn;                //转换按钮

    //width缩放比例
    private float widthZoomMultiple = 0f;
    //height缩放比例
    private float heightZoomMultiple = 0f;

    /** 动画时长 */
    private static final int ANIMATOR_DURATION = 300;
    //设置Interpolator，动画效果上更接近小米计算器动画效果
    private Interpolator interpolator = new DecelerateInterpolator();

    /**
     * 启动Activity
     * @param context context
     */
    public static void startSelf(Context context){
        Intent intent = new Intent(context, XiaomiActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_xiaomi);

        ButterKnife.bind(this);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if(hasFocus){
            setLayoutTransition();
        }
    }

    @OnClick(R.id.change_btn)
    protected void onClick(){
        if(widthZoomMultiple == 0 || heightZoomMultiple == 0){
            //获取缩放比例
            widthZoomMultiple = getWidthZoomMultiple();
            heightZoomMultiple = getHeightZoomMultiple();
        }

        if(calculatorTopLin.getVisibility() == View.VISIBLE){
            calculatorTopLin.setVisibility(View.GONE);
            calculatorLeftLin.setVisibility(View.GONE);

            startEnlargeAnimator();
        } else{
            calculatorTopLin.setVisibility(View.VISIBLE);
            calculatorLeftLin.setVisibility(View.VISIBLE);

            startNarrowAnimator();
        }
    }

    /**
     * 获取width缩放比例
     * @return
     */
    private float getWidthZoomMultiple(){
        return (float) calculatorRel.getWidth() / calculatorNumLin.getWidth();
    }

    /**
     * 获取height缩放比例
     * @return
     */
    private float getHeightZoomMultiple(){
        return (float) calculatorRel.getHeight() / calculatorNumLin.getHeight();
    }

    /**
     * 放大动画
     */
    private void startEnlargeAnimator(){
        calculatorRel.setPivotX(calculatorRel.getWidth());
        calculatorRel.setPivotY(calculatorRel.getHeight());

        AnimatorSet animatorSet = new AnimatorSet();
        //放大动画
        Animator enlargeAnimatorX = ObjectAnimator.ofFloat(calculatorRel,"scaleX",1,widthZoomMultiple);
        Animator enlargeAnimatorY = ObjectAnimator.ofFloat(calculatorRel,"scaleY",1,widthZoomMultiple);

        animatorSet.setDuration(ANIMATOR_DURATION);
        animatorSet.play(enlargeAnimatorX).with(enlargeAnimatorY);
        animatorSet.setInterpolator(interpolator);
        animatorSet.start();
    }

    /**
     * 缩小动画
     */
    private void startNarrowAnimator(){
        calculatorRel.setPivotX(calculatorRel.getWidth());
        calculatorRel.setPivotY(calculatorRel.getHeight());

        AnimatorSet animatorSet = new AnimatorSet();
        //缩小动画
        Animator narrowAnimatorX = ObjectAnimator.ofFloat(calculatorRel,"scaleX",widthZoomMultiple, 1);
        Animator narrowAnimatorY = ObjectAnimator.ofFloat(calculatorRel,"scaleY",widthZoomMultiple, 1);
        animatorSet.setDuration(ANIMATOR_DURATION);
        animatorSet.play(narrowAnimatorX).with(narrowAnimatorY);
        animatorSet.setInterpolator(interpolator);
        animatorSet.start();
    }

    /**
     * 设置切换动画
     */
    private void setLayoutTransition(){
        LayoutTransition mTransition = new LayoutTransition();
        mTransition.setDuration(LayoutTransition.DISAPPEARING,ANIMATOR_DURATION);
        mTransition.setDuration(LayoutTransition.APPEARING, ANIMATOR_DURATION);

        mTransition.setStagger(LayoutTransition.DISAPPEARING, 0);
        mTransition.setStagger(LayoutTransition.APPEARING, 0);

        //View移除动画
        PropertyValuesHolder alphaHolder = PropertyValuesHolder.ofFloat("alpha",1, 0);
        ObjectAnimator disAppearingAnimator = ObjectAnimator.ofPropertyValuesHolder(this,alphaHolder);
        disAppearingAnimator.setInterpolator(interpolator);
        disAppearingAnimator.setDuration(mTransition.getDuration(LayoutTransition.DISAPPEARING));
        mTransition.setAnimator(LayoutTransition.DISAPPEARING, disAppearingAnimator);

        //View显示动画
        PropertyValuesHolder alphaAppearingHolder = PropertyValuesHolder.ofFloat("alpha",0, 1);
        ObjectAnimator appearingAnimator = ObjectAnimator.ofPropertyValuesHolder(this,alphaAppearingHolder);
        appearingAnimator.setInterpolator(interpolator);
        appearingAnimator.setDuration(mTransition.getDuration(LayoutTransition.APPEARING));
        mTransition.setAnimator(LayoutTransition.APPEARING, appearingAnimator);

        calculatorRel.setLayoutTransition(mTransition);
    }
}
