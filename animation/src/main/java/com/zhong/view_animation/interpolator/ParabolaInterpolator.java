package com.zhong.view_animation.interpolator;

import android.view.animation.Interpolator;

/**
 * <pre>
 *     author: Zhong
 *     time  : 2019/3/25
 *     desc  : 抛物线Interpolator
 * </pre>
 */
public class ParabolaInterpolator implements Interpolator {
    @Override
    public float getInterpolation(float input) {
        return input * input;
    }
}
