package com.zhong.property_animation;

import android.util.Log;

/**
 * <pre>
 *     author: Zhong
 *     time  : 2019/3/29
 *     desc  :
 * </pre>
 */
public class CustomObject {
    private static final String TAG = CustomObject.class.getSimpleName();

    private int customParam;

    public CustomObject() {
    }

    public CustomObject(int customParam) {
        this.customParam = customParam;
    }

    public int getCustomParam() {
        return customParam;
    }

    public void setCustomParam(int customParam) {
        Log.d(TAG,"CustomObject setter param " + customParam);
        this.customParam = customParam;
    }
}
