package com.zhong.demo.butterknife;

import android.content.Context;
import android.content.Intent;

import com.zhong.demo.butterknife.base.BaseActivity;

public class BindFragmentActivity extends BaseActivity {

    public static void startSelf(Context context){
        Intent intent = new Intent(context, BindFragmentActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected int initLayout() {
        return com.zhong.demo.butterknife.R.layout.activity_bind_view;
    }

    @Override
    protected void create() {

    }
}
