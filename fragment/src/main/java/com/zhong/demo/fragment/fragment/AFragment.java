package com.zhong.demo.fragment.fragment;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.zhong.demo.fragment.R;
import com.zhong.demo.fragment.TVTabLayoutActivity;
import com.zhong.demo.fragment.fragment.base.BaseLazyLoadFragment;

public class AFragment extends BaseLazyLoadFragment {
    private Button testBtn;

    @Override
    protected void initView() {
        testBtn = view.findViewById(R.id.start_test_activity_btn);

        testBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), TVTabLayoutActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected int initLayout() {
        return R.layout.fragment_a;
    }

    @Override
    protected void lazyLoad() {
        Log.d(TAG,getClass().getSimpleName() + " lazyLoad");
    }

    @Override
    protected void visibleReLoad() {
        Log.d(TAG,getClass().getSimpleName() + " visibleReLoad");
    }

    @Override
    protected void inVisibleRelease() {
        Log.d(TAG,getClass().getSimpleName() + " inVisibleRelease");
    }
}
