package com.zhong.media.media.player;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Surface;
import android.view.TextureView;
import android.view.ViewGroup;

import com.zhong.media.media.Utils;

import java.io.IOException;
import java.util.Map;


@SuppressLint("NewApi")
public class MyTextureView extends TextureView implements TextureView.SurfaceTextureListener, IPlayer {
    private static final String TAG = MyTextureView.class.getSimpleName();

    private MediaPlayer mMediaPlayer;
    private Surface surface;

    private Context context;
    // settable by the client
    private Uri mUri;
    private Map<String, String> mHeaders;

    private MediaPlayer.OnPreparedListener onPreparedListener;

    public MyTextureView(Context context) {
        this(context, null);
    }

    public MyTextureView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;

        this.setSurfaceTextureListener(this);
    }

    @Override
    public void setVideoPath(String videoPath) {
        setVideoURI(Uri.parse(videoPath));
    }

    @Override
    public void setVideoURI(Uri uri) {
        setVideoURI(uri, null);
    }

    @Override
    public void setVideoURI(Uri uri, Map<String, String> headers) {
        mUri = uri;
        mHeaders = headers;

        openVideo();
    }

    private void openVideo() {
        if (mUri == null || surface == null) {
            // not ready for playback just yet, will try again later
            Log.d(TAG, "mUri == null or surface == null");
            return;
        }
        // we shouldn't clear the target state, because somebody might have
        // called start() previously
        release();

        try {
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setOnPreparedListener(mPreparedListener);
//            mMediaPlayer.setOnVideoSizeChangedListener(mSizeChangedListener);
//            mMediaPlayer.setOnCompletionListener(mCompletionListener);
//            mMediaPlayer.setOnErrorListener(mErrorListener);
//            mMediaPlayer.setOnInfoListener(mInfoListener);
//            mMediaPlayer.setOnBufferingUpdateListener(mBufferingUpdateListener);
            mMediaPlayer.setDataSource(context, mUri, mHeaders);
            mMediaPlayer.setSurface(surface);
            mMediaPlayer.setScreenOnWhilePlaying(true);

            //使用prepareAsync必须在onPrepared之后调用mMediaPlayer.start()
            mMediaPlayer.prepareAsync();
//            mMediaPlayer.prepare();
//            mMediaPlayer.start();

        } catch (IOException ex) {
            Log.w(TAG, "Unable to open content: " + mUri, ex);
        } catch (IllegalArgumentException ex) {
            Log.w(TAG, "Unable to open content: " + mUri, ex);
        }
    }

    MediaPlayer.OnPreparedListener mPreparedListener = new MediaPlayer.OnPreparedListener() {
        @Override
        public void onPrepared(MediaPlayer mp) {
            start();

            if(onPreparedListener != null){
                onPreparedListener.onPrepared(mp);
            }
        }
    };

    @Override
    public void start() {
        if (mMediaPlayer != null) {
            mMediaPlayer.start();
        }
    }

    @Override
    public void pause() {
        if(mMediaPlayer != null){
            if(mMediaPlayer.isPlaying()){
                mMediaPlayer.pause();
            }
        }
    }

    @Override
    public void stop() {
        if (mMediaPlayer != null) {
            mMediaPlayer.stop();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    @Override
    public void release() {
        try {
            if (mMediaPlayer != null) {
                mMediaPlayer.reset();
                mMediaPlayer.release();
                mMediaPlayer = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isPlaying() {
        if(mMediaPlayer != null){
            return mMediaPlayer.isPlaying();
        } else{
            return false;
        }
    }

    @Override
    public void setOnPreparedListener(MediaPlayer.OnPreparedListener onPreparedListener) {
        this.onPreparedListener = onPreparedListener;
    }

    @Override
    public MediaPlayer getMediaPlayer() {
        return null;
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int width, int height) {
        surface = new Surface(surfaceTexture);

        openVideo();
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        release();
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {

    }
}
