package com.zhong.other_animation.transition;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.zhong.main.BaseActivity;
import com.zhong.main.R;

import butterknife.BindView;
import butterknife.OnClick;

public class TransitionActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    public static void startSelf(Context context) {
        Intent intent = new Intent(context, TransitionActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected int initLayout() {
        return R.layout.activity_transition;
    }

    @Override
    protected void create() {
        initToolbar();
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick({R.id.scene_transition_btn, R.id.activity_transition_btn, R.id.begin_transition_btn})
    protected void onClick(View view) {
        switch (view.getId()) {
            case R.id.scene_transition_btn:
                SceneTransitionActivity.startSelf(TransitionActivity.this);
                break;
            case R.id.activity_transition_btn:
                ActivityTransitionsActivity.startSelf(TransitionActivity.this);
                break;
            case R.id.begin_transition_btn:
                BeginTransitionActivity.startSelf(TransitionActivity.this);
                break;
        }
    }
}
