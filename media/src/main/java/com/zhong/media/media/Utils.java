package com.zhong.media.media;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Point;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.DisplayMetrics;

public class Utils {
    /**
     * 获取屏幕分辨率
     * @param context
     * @return
     */
    public static Point getScreenSize(Context context) {
        Point screenSize = new Point();
//	        WindowManager wm = (WindowManager) (context.getSystemService(Context.WINDOW_SERVICE));
//	        DisplayMetrics dm = new DisplayMetrics();
//	        wm.getDefaultDisplay().getMetrics(dm);
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        screenSize.x = dm.widthPixels;
        screenSize.y = dm.heightPixels;
        return screenSize;
    }

    /**
     * 获取屏幕宽度
     * @param context
     * @return
     */
    public static int getScreenWidth(Context context) {
//	        WindowManager wm = (WindowManager) (context.getSystemService(Context.WINDOW_SERVICE));
//	        DisplayMetrics dm = new DisplayMetrics();
//	        wm.getDefaultDisplay().getMetrics(dm);
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        return dm.widthPixels;
    }

    /**
     * 获取屏幕高度
     * @param context
     * @return
     */
    public static int getScreenHeight(Context context) {
//	        WindowManager wm = (WindowManager) (context.getSystemService(Context.WINDOW_SERVICE));
//	        DisplayMetrics dm = new DisplayMetrics();
//	        wm.getDefaultDisplay().getMetrics(dm);
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        return dm.heightPixels;
    }

    /**
     * 返回Uri的真实路径
     * @param context
     * @param uri
     * @return
     */
    public static String getFilePath(final Context context, final Uri uri) {
        if (null == uri) return null;

        final String scheme = uri.getScheme();
        String data = null;

        if (scheme == null)
            data = uri.getPath();
        else if (ContentResolver.SCHEME_FILE.equals(scheme)) {
            data = uri.getPath();
        } else if (ContentResolver.SCHEME_CONTENT.equals(scheme)) {
            Cursor cursor = context.getContentResolver().query(uri, new String[]{MediaStore.Images.ImageColumns.DATA}, null, null, null);
            if (null != cursor) {
                if (cursor.moveToFirst()) {
                    int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                    if (index > -1) {
                        data = cursor.getString(index);
                    }
                }
                cursor.close();
            }
        }
        return data;
    }
}
