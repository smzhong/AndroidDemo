package com.zhong.other_animation;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.zhong.main.BaseActivity;
import com.zhong.main.R;
import com.zhong.main.adapter.AnimationAdapter;
import com.zhong.other_animation.transition.TransitionActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class OtherAnimationActivity extends BaseActivity {

    private List<String> datas;
    private AnimationAdapter adapter;

    @BindView(R.id.recycler_view)
    protected RecyclerView mRecyclerView;

    public static void startSelf(Context context){
        Intent intent = new Intent(context, OtherAnimationActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected int initLayout() {
        return R.layout.activity_other_animation;
    }

    @Override
    protected void create() {
        initData();
        initRecyclerView();
    }

    private void initData(){
        datas = new ArrayList<>();
        datas.add("Touch Feedback");
        datas.add("RevealEffect");
        datas.add("Transitions");
    }
    private void initRecyclerView() {
        adapter = new AnimationAdapter(OtherAnimationActivity.this, datas);
        adapter.setOnClickListener(onItemClickListener);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(OtherAnimationActivity.this, 2);
        gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(gridLayoutManager);
        mRecyclerView.setAdapter(adapter);
    }


    AnimationAdapter.OnItemClickListener onItemClickListener = new AnimationAdapter.OnItemClickListener() {
        @Override
        public void onClick(int position, View view) {
            switch (position){
                case 0:
                    TouchFeedbackActivity.startSelf(OtherAnimationActivity.this);
                    break;
                case 1:
                    RevealEffectActivity.startSelf(OtherAnimationActivity.this);
                    break;
                case 2:
                    TransitionActivity.startSelf(OtherAnimationActivity.this);
                    break;
            }
        }
    };
}
