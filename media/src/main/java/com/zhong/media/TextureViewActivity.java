package com.zhong.media;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.zhong.media.media.media2.MyTextureView2;


public class TextureViewActivity extends Activity {
	private String url;
	private MyTextureView2 surfaceView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_texture_view);

		Intent intent = getIntent();
		if(intent != null){
			url = intent.getStringExtra("url");
		}

		surfaceView = (MyTextureView2) findViewById(R.id.surface_view);

		surfaceView.setVideoPath(url);
		surfaceView.startVideo();
		surfaceView.setOnPlayerCallback(null);
	}
}
