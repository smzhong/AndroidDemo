package com.zhong.media;

import android.app.Application;
import android.content.Context;

import com.danikula.videocache.HttpProxyCacheServer;

/**
 * <pre>
 *     author: Zhong
 *     time  : 2020/3/11
 *     desc  :
 * </pre>
 */
public class MyApplication extends Application {
    private HttpProxyCacheServer proxy;

    public static HttpProxyCacheServer getProxy(Context context) {
        MyApplication app = (MyApplication) context.getApplicationContext();
        return app.proxy == null ? (app.proxy = app.newProxy()) : app.proxy;
    }

    private HttpProxyCacheServer newProxy() {
        return new HttpProxyCacheServer.Builder(this)
                .cacheDirectory(Utils.getVideoCacheDir(this))
                .maxCacheSize(100 * 1024 * 1024)       // 100 M for cache
                .maxCacheFilesCount(20)                // 最大缓存20个视频
                .build();
    }
}
